# OS LAB 2 Notes
## Exercises
1. Redirect the output of `df` to a file called `diskspaces.txt`.
```
$ df | cat > diskspaces.txt
$ df > diskspaces.txt
```
2. Create a file called `status.txt` that contains current date, current running programs and disk spaces.
```
$ date > status.txt
$ ps aux >> status.txt
$ df >> status.txt
```
3. Select all files and folders that are created at August in the current directory.
```
$ ls -l | grep "Aug"
```
4. To prevent the zipped out output of a `ls` command the following command may be used.
```
$ ls -l | less
```
5. Run a command in background.
```
$ gedit &
```
6. Run two commands simultaneously.
```
$ mkdir mydir ; cd mydir ; touch text.txt ; echo "This is a test" > text.txt
```
7. Run the second command only if the first command is successful.
```
$ ping -c3 www.google.com && curl www.google.com
```
8. If the first command fails, output a fail message.
```
$ ping -c3 moodle.atilim.edu.tr || echo "Cannot reach to the Moodle via pings."
```
9. Write a command that checks the Moodle. Print `Verified` if the server is up, else `Host is down`.
```
$ ping -c3 moodle.atilim.edu.tr && echo "Verified" || echo "Host is down"
```
10. Write sequence of commands that does the following:
    - Create a directory and switch to that directory.
    - Create 8 files with different extensions (at least 2 of them should be html files)
    - Remove all the files except html ones.
```
$ mkdir test2
$ cd test2
$ touch a.txt b.txt c.html d.html e.json f.pdf g.pdf h.xml
$ ls
$ rm -r !(*.html)
$ ls
```
11. Find all the rows that start with all capital letters through A to K.
```
$ cat datas.txt | grep '^[A-K]'
```
12. Find all rows that start with capital A to K and the second word start with w.
```
$ cat datas.txt | grep '^[A-K][a-k]* w'
```
13. Find the row that ends with 20.
```
$ cat datas.txt | grep '20$'
```
14. Repeat the question 11 and count the matching rows.
```
$ cat datas.txt | grep -c '^[A-K]'
```
15. Find your username inside passwd file.
```
$ grep tustunkok /etc/passwd
```