/**
 * dining_philosophers.c
 * -----------------
 * Thread and Semaphore Examples
 * Julie Zelenski, Nick Parlante
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>
#include <unistd.h>

#define NUM_DINNERS 5
#define EAT_TIMES 3

#define LEFT(philNum) (philNum)
#define RIGHT(philNum) (((philNum) + 1) % NUM_DINNERS)

struct thread_data {
    char name[32];
    int philNum;
};

sem_t fork_sem[NUM_DINNERS], numEating;

void think(char *threadName) {
    printf("%s thinking!\n", threadName);
    int sleepTime = sleep((rand() % 5) + 1); 
    sleep(sleepTime);
}

void eat(struct thread_data *data) {
    sem_wait(&numEating);
    sem_wait(&fork_sem[LEFT(data->philNum)]);
    sem_wait(&fork_sem[RIGHT(data->philNum)]);

    printf("%s eating!\n", data->name);
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);

    sem_post(&fork_sem[LEFT(data->philNum)]);
    sem_post(&fork_sem[RIGHT(data->philNum)]);
    sem_post(&numEating);
}

void* philosopher(void *args) {
    struct thread_data *data = (struct thread_data*) args;

    for (int i = 0; i < EAT_TIMES; i++) {
        think(data->name);
        eat(data);
    }

    free(data);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t philosophers[NUM_DINNERS];
    srand(time(NULL));

    for (int i = 0; i < NUM_DINNERS; i++) {
        sem_init(&fork_sem[i], 0, 1); // Fork mutexes
    }
    sem_init(&numEating, 0, NUM_DINNERS - 1);

    for (int i = 0; i < NUM_DINNERS; i++) {
        struct thread_data *data = malloc(sizeof(struct thread_data));
        sprintf(data->name, "Philosopher %d", i);
        data->philNum = i;
        pthread_create(&philosophers[i], NULL, philosopher, data);
    }

    for (int i = 0; i < NUM_DINNERS; i++) {
        pthread_join(philosophers[i], NULL);
    }

    printf("All done!\n");

    for (int i = 0; i < NUM_DINNERS; i++) {
        sem_destroy(&fork_sem[i]);
    }
    sem_destroy(&numEating);

    return 0;
}