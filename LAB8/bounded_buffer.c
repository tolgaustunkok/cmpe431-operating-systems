#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define BUFFER_SIZE 10

int count = 0;
char buffer[BUFFER_SIZE];
sem_t mutex, empty, full;

void* producer(void *args) {
    sem_wait(&empty);

    printf("Producing function.\n");

    if (count >= BUFFER_SIZE) {
        printf("'count' is larger than buffer.\n");
    } else {
        printf("\n");
        for (int i = 0; i < BUFFER_SIZE; i++) {
            sem_wait(&mutex);

            buffer[i] = i;
            printf(" %d ", buffer[count]);
            count++;

            sem_post(&mutex);
        }
        printf("\n");
    }

    sem_post(&full);
    pthread_exit(NULL);
}

void* consumer(void *args) {
    sem_wait(&full);

    printf("Consuming function.\n");

    if (count < 0) {
        printf("Count is less than 0.\n");
    } else {
        printf("\n");
        for (int i = 0; i < BUFFER_SIZE; i++) {
            sem_wait(&mutex);

            printf(" %d ", buffer[count - 1]);
            count--;

            sem_post(&mutex);
        }
        printf("\n");
    }

    sem_post(&empty);
}

int main(void) {
    pthread_t producer_tid, consumer_tid;
    sem_init(&mutex, 0, 1);
    sem_init(&empty, 0, BUFFER_SIZE);
    sem_init(&full, 0, 0);

    pthread_create(&producer_tid, NULL, producer, NULL);
    pthread_create(&consumer_tid, NULL, consumer, NULL);

    pthread_join(producer_tid, NULL);
    pthread_join(consumer_tid, NULL);

    sem_destroy(&mutex);
    sem_destroy(&empty);
    sem_destroy(&full);
    return 0;
}