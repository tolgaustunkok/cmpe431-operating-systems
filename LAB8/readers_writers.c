/**
 * readers_writers.c
 * -----------------
 * Thread and Semaphore Examples
 * Julie Zelenski, Nick Parlante
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#define NUM_TOTAL_BUFFERS 5
#define DATA_LENGTH 20

sem_t empty, full;
char buffers[NUM_TOTAL_BUFFERS];

char prepareData() {
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);
    char letter = (rand() % 26) + 65;
    return letter;
}

void processData() {
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);
}

void* writer_routine(void *args) {
    int writePt = 0;
    char data;

    for (int i = 0; i < DATA_LENGTH; i++) {
        data = prepareData();
        sem_wait(&empty);
        buffers[writePt] = data;
        printf("%s: buffer[%d] = %c\n", "Writer", writePt, data);
        writePt = (writePt + 1) % NUM_TOTAL_BUFFERS;
        sem_post(&full);
    }

    pthread_exit(NULL);
}

void* reader_routine(void *args) {
    int readPt = 0;
    char data;

    for (int i = 0; i < DATA_LENGTH; i++) {
        sem_wait(&full);
        data = buffers[readPt];
        printf("\t\t%s: buffer[%d] = %c\n", "Reader", readPt, data);
        readPt = (readPt + 1) % NUM_TOTAL_BUFFERS;
        sem_post(&empty);
        processData();
    }

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t writer, reader;
    srand(time(NULL));

    sem_init(&empty, 0, NUM_TOTAL_BUFFERS);
    sem_init(&full, 0, 0);

    pthread_create(&writer, NULL, writer_routine, NULL);
    pthread_create(&reader, NULL, reader_routine, NULL);

    pthread_join(writer, NULL);
    pthread_join(reader, NULL);

    sem_destroy(&empty);
    sem_destroy(&full);

    printf("All done!\n");
    return 0;
}
