#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define NUM_LOOPS 200000000

int sum = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* counting_function(void *args) {
    int offset = *((int*) args);
    for (int i = 0; i < NUM_LOOPS; i++) {
        // Critical section begins
        pthread_mutex_lock(&mutex);
        sum += offset;
        //pthread_mutex_unlock(&mutex);
        // Critical section ends
    }

    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid1, tid2;
    int offset1 = 1, offset2 = -1;

    pthread_create(&tid1, NULL, counting_function, &offset1);
    pthread_create(&tid2, NULL, counting_function, &offset2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Sum is %d\n", sum);

    pthread_mutex_destroy(&mutex);
    return 0;
}