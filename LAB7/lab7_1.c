#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ARRAY_LENGTH 100
#define THREAD_COUNT 5

struct sum_t {
    int numSequence[ARRAY_LENGTH / THREAD_COUNT];
    int result;
};

void* calculate(void *args) {
    struct sum_t *results = (struct sum_t*) args;
    results->result = 0;

    for (int i = 0; i < ARRAY_LENGTH / THREAD_COUNT; i++) {
        results->result += results->numSequence[i];
    }

    pthread_exit(NULL);
}

int main(void) {
    int sum = 0;
    pthread_t tids[THREAD_COUNT];
    int numbers[ARRAY_LENGTH];
    struct sum_t results[THREAD_COUNT];

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        numbers[i] = i;
    }

    for (int i = 0; i < THREAD_COUNT; i++) {
        for (int j = 0; j < ARRAY_LENGTH / THREAD_COUNT; j++) {
            results[i].numSequence[j] = numbers[j + i * (ARRAY_LENGTH / THREAD_COUNT)];
        }

        pthread_create(&tids[i], NULL, calculate, &results[i]);
    }

    for (int i = 0; i < THREAD_COUNT; i++) {
        pthread_join(tids[i], NULL);
        printf("Result of thread %d is %d.\n", i, results[i].result);
        sum += results[i].result;
    }

    printf("Sum is %d\n", sum);
    return 0;
}