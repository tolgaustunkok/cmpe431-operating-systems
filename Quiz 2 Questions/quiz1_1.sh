#!/bin/bash

echo "Enter binary numbers:"
read -a numbers

function bin2dec() {
    tempNum=$1
    result=0
    count=0

    while [[ $tempNum -gt 0 ]]
    do
        lastDigit=$(( tempNum % 10 ))
        result=$(( result + lastDigit * 2**count ))
        count=$(( count + 1 ))
        tempNum=$(( tempNum / 10 ))
    done

}

for num in "${numbers[@]}"
do
    bin2dec $num

    if [[ $(( result % 5 )) == 0 ]]
    then
        echo -e "$num,\c" >> output.txt
    fi
done

echo "Finished!"
