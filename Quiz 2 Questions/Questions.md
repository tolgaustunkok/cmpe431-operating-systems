# Quiz 2 - 22.11.2018

## Questions
**Question 1.** Write a bash script that prints a number in reverse order.

* The script should input the number as an argument. (**20pts**)
* If no argument or more than one argument is supplied, the script give an error message (15pts) to the `stderr` file descriptor instead of `stdout` (10pts) and terminates with a non-zero exit status (5pts). (**30pts**)
* The reversing operation should be done inside a function called `reverser` and the number to be reversed should be passed as a parameter to the function (10pts). (**30pts**)
* The function should return the result to the rest of the script. In other words, the printing of the result should be done in script itself, **not in function**. (**20pts**)

**Sample run:**

```
$ ./reverse.sh 1234
Reversed number is 4321.
$ ./reverse.sh 548
Reversed number is 845.
$ ./reverse.sh
Please provide the correct number of inputs.
Usage: ./reverse number
```

**Answer:**

```bash
#! /bin/bash

if [ $# -ne 1 ]
then
    echo "Please provide the correct number of inputs." >&2
    echo "Usage: $0 number" >&2
    exit 1
fi

function reverser() {
    num=$1
    lastDigit=0
    reversedNum=0

    while [ $num -gt 0 ]
    do
        lastDigit=$(expr $num % 10)
        reversedNum=$(expr $reversedNum \* 10 + $lastDigit)
        num=$(expr $num / 10)
    done

    echo "$reversedNum"
}

reverser $1
echo "Reversed number is $reversedNum"
```

**Question 2.** Write a bash script that finds the numbers inside `.txt` files in the current directory and takes the average of them. There is at most one number in each line in a file. However, there may be infinite number of numbers in each file. Use the files that is provided in the Moodle for testing purposes.

* There should be a function called `average` that calculates the average. (**70pts**)
* The printing operation of the result should not be performed in the function. (**30pts**)

**Sample run:**
```
$ ./average.sh
Average is 46.
```

**Answer:**

```bash
#! /bin/bash

function average() {
    sum=0
    numCount=0

    for file in *.txt
    do
        while read line
        do
            sum=$(expr $sum + $line)
            numCount=$(expr $numCount + 1)
        done < "$file"
    done

    result=$(expr $sum / $numCount)
}

average
echo "Average is $result"
```

**Question 3.** Write a script which will find all such numbers which are divisible by 7 but not a multiple of 5, between two user defined numbers inclusive.

* If no argument provided to the script give an error message (15pts) and usage information to `stderr` (10pts). Then exit with fail status (5pts). (**30pts**)
* The numbers obtained should be written out to a file called `output.txt` one number for each line. (**30pts**)
* Define the file name (`output.txt`) as a constant. (**10pts**)
* All these operations must be performed in a function. In other words, the script only calls the function in addition to the implementation of the function. (**30pts**)
* Upload both script and `output.txt` to the moodle.

**Hint:** Don't forget to call the function. :wink:

**Sample run:**
```
$ ./quiz2 5000 8000
Finished!
$ ./quiz2
Usage: ./quiz2 num1 num2
```

**Answer:**

```bash
#!/bin/bash

readonly fileName="output.txt"

if [[ $# -ne 2 ]]
then
    echo "Usage: $0 num1 num2" >&2
    exit 1
fi

function findNumbers() {
    for (( i=$1; i<=$2; i++ ))
    do
        if [[ $(( i % 7 )) == 0 && $(( i % 5 )) != 0 ]]
        then
            echo "$i" >> $fileName
        fi
    done
}

findNumbers $1 $2
echo "Finished!"
```

**Question 4.** Write a script which accepts a sequence of space delimited binary numbers as its input (10pts) and then check whether they are divisible by 5 or not (50pts). The numbers that are divisible by 5 are to be printed in a file called `output.txt` as a comma seperated sequence (20pts) in binary format (20pts).

**Hint:** You can take the exponent of a number with `**` operator. For example: `2^5` can be written as `2**5`. Otherwise, you can always implement your own exponent function with a loop. But it will take a little longer time to write it compared to the explained one.

**Sample run:**

```
$ ./quiz2.sh
Enter binary numbers:
100 11 1010 1001 11001
Finished!
```

**Contents of output.txt**
```
1010,11001,
```

**Answer:**

```bash
#!/bin/bash

function bin2dec() {
    tempNum=$1
    result=0
    count=0

    while [[ $tempNum -gt 0 ]]
    do
        lastDigit=$(( tempNum % 10 ))
        result=$(( result + lastDigit * 2**count ))
        count=$(( count + 1 ))
        tempNum=$(( tempNum / 10 ))
    done

}

echo "Enter binary numbers:"
read -a numbers

for num in "${numbers[@]}"
do
    bin2dec $num

    if [[ $(( result % 5 )) == 0 ]]
    then
        echo -e "$num,\c" >> output.txt
    fi
done

echo "Finished!"
```
