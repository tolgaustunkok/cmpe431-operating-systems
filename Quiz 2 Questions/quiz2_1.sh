#! /bin/bash

if [ $# -ne 1 ]
then
    echo "Please provide the correct number of inputs." >&2
    echo "Usage: $0 number" >&2
    exit 1
fi

function reverser() {
    num=$1
    lastDigit=0
    reversedNum=0

    while [ $num -gt 0 ]
    do
        lastDigit=$(expr $num % 10)
        reversedNum=$(expr $reversedNum \* 10 + $lastDigit)
        num=$(expr $num / 10)
    done

    echo "$reversedNum"
}

reverser $1
echo "Reversed number is $reversedNum"