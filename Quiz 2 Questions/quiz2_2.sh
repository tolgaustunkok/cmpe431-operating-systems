#!/bin/bash

readonly fileName="output.txt"

if [[ $# -ne 2 ]]
then
    echo "Usage: $0 num1 num2" >&2
    exit 1
fi

function findNumbers() {
    for (( i=$1; i<=$2; i++ ))
    do
        if [[ $(( i % 7 )) == 0 && $(( i % 5 )) != 0 ]]
        then
            echo "$i" >> $fileName
        fi
    done
}

findNumbers $1 $2
echo "Finished!"