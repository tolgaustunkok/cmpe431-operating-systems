#! /bin/bash

function average() {
    sum=0
    numCount=0

    for file in *.txt
    do
        while read line
        do
            sum=$(expr $sum + $line)
            numCount=$(expr $numCount + 1)
        done < "$file"
    done

    result=$(expr $sum / $numCount)
}

average
echo "Average is $result"