# <center>Atılım University<br>Department of Computer Engineering</center>
## <center>CMPE 431 Operating Systems Lab Quiz 2</center>
## <center>Nov. 21st, 2018</center>
### Duration: 30 Minutes<br>Number of pages: 1 page

---
**WARNINGS**

* It is forbidden to bring electronic data storage equipments (e.g. mobile phone, MP3 player, flash disk etc.) to exams. 
* Students who either cheat, attempt to cheat or provide a help to other(s) in cheating, get 0 (zero) grade from the entered examination. Also,  based on the regulations, a disciplinary action will be taken. 

---
<br>

**Question.** Write a bash script that prints a number in reverse order.

* The script should input the number as an argument. (**20pts**)
* If no argument or more than one argument is supplied, the script give an error message (15pts) to the `stderr` file descriptor instead of `stdout` (10pts) and terminates with a non-zero exit status (5pts). (**30pts**)
* The reversing operation should be done inside a function called `reverser` and the number to be reversed should be passed as a parameter to the function (10pts). (**30pts**)
* The function should return the result to the rest of the script. In other words, the printing of the result should be done in script itself, **not in function**. (**20pts**)

**Sample run:**

```
$ ./reverse.sh 1234
Reversed number is 4321.
$ ./reverse.sh 548
Reversed number is 845.
$ ./reverse.sh
Please provide the correct number of inputs.
Usage: ./reverse.sh number