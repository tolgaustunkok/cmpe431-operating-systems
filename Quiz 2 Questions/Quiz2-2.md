# <center>Atılım University<br>Department of Computer Engineering</center>
## <center>CMPE 431 Operating Systems Lab Quiz 2</center>
## <center>Nov. 21st, 2018</center>
### Duration: 30 Minutes<br>Number of pages: 1 page

---
**WARNINGS**

* It is forbidden to bring electronic data storage equipments (e.g. mobile phone, MP3 player, flash disk etc.) to exams. 
* Students who either cheat, attempt to cheat or provide a help to other(s) in cheating, get 0 (zero) grade from the entered examination. Also,  based on the regulations, a disciplinary action will be taken. 

---
<br>

**Question.** Write a script which will find all such numbers which are divisible by 7 but not a multiple of 5, between two user defined numbers inclusive.

* If no argument provided to the script give an error message (15pts) and usage information to `stderr` (10pts). Then exit with fail status (5pts). (**30pts**)
* The numbers obtained should be written out to a file called `output.txt` one number for each line. (**30pts**)
* Define the file name (`output.txt`) as a constant. (**10pts**)
* All these operations must be performed in a function. In other words, the script only calls the function in addition to the implementation of the function. (**30pts**)
* Upload both script and `output.txt` to the moodle.

**Hint:** Don't forget to call the function. :wink:

**Sample run:**
```
$ ./quiz2.sh 5000 8000
Finished!
$ ./quiz2.sh
There should be at least two numbers.
Usage: ./quiz2 num1 num2
```