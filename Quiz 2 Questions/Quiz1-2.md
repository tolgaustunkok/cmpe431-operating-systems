# <center>Atılım University<br>Department of Computer Engineering</center>
## <center>CMPE 431 Operating Systems Lab Quiz 2</center>
## <center>Nov. 22nd, 2018</center>
### Duration: 30 Minutes<br>Number of pages: 1 page

---
**WARNINGS**

* It is forbidden to bring electronic data storage equipments (e.g. mobile phone, MP3 player, flash disk etc.) to exams. 
* Students who either cheat, attempt to cheat or provide a help to other(s) in cheating, get 0 (zero) grade from the entered examination. Also,  based on the regulations, a disciplinary action will be taken. 

---
<br>

**Question.** Write a bash script that finds the numbers inside `.txt` files (assume that files consist of only numbers) in the current directory and takes the average of them. There is at most one number in each line in a file. However, there may be infinite number of numbers in each file. Use the files that is provided in the Moodle for testing purposes.

* There should be a function called `average` that calculates the average. (**70pts**)
* The printing operation of the result should not be performed in the function. (**30pts**)

**Sample run:**
```
$ ./average.sh
Average is 46.
```