# <center>Atılım University<br>Department of Computer Engineering</center>
## <center>CMPE 431 Operating Systems Lab Quiz 2</center>
## <center>Nov. 22nd, 2018</center>
### Duration: 30 Minutes<br>Number of pages: 1 page

---
**WARNINGS**

* It is forbidden to bring electronic data storage equipments (e.g. mobile phone, MP3 player, flash disk etc.) to exams. 
* Students who either cheat, attempt to cheat or provide a help to other(s) in cheating, get 0 (zero) grade from the entered examination. Also,  based on the regulations, a disciplinary action will be taken. 

---
<br>

**Question.** Write a script which accepts a sequence of space delimited binary numbers as its input (10pts) and then check whether they are divisible by 5 or not (50pts). The numbers that are divisible by 5 are to be printed in a file called `output.txt` as a comma seperated sequence (20pts) in binary format (20pts).

**Hint:** You can take the exponent of a number with `**` operator. For example: `2^5` can be written as `2**5`. Otherwise, you can always implement your own exponent function with a loop. But it will take a little longer time to write it compared to the explained one.

**Sample run:**

```
$ ./quiz2.sh
Enter binary numbers:
100 11 1010 1001 11001
Finished!
```

**Contents of output.txt**
```
1010,11001,
```