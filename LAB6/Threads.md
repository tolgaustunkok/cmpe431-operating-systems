# LAB6 - Threads
## Threads in GNU/Linux
The following quote from the book *Linux Kernel Development, 3rd Edition* gives a great explanation of Linux threads.

> ...Linux has a unique implementation of threads. To the Linux kernel, there is no concept of a thread. Linux implements all threads as standard processes. The Linux kernel does not provide any special scheduling semantics or data structures to represent threads. Instead, a thread is merely a process that shares certain resources with other processes. Each thread has a unique task_struct and appears to the kernel as a normal process (which just happens to share resources, such as an address space, with other processes)...

And this quote continues with an example of a multi-threaded program.

> ...For example, assume you have a process that consists of four threads. On systems with explicit thread support (such as Windows, Solaris, ...), there might exist one process descriptor that in turn points to the four different threads. The process descriptor describes the shared resources, such as an address space or open files. The threads then describe the resources they alone possess. Conversely, in Linux, there are simply four processes and thus four normal `task_struct` structures. The four processes are set up to share certain resources...

## POSIX Threads (pthread)
Pthread is a standard model for creating parallel subtasks. It comes with a C header file to include and a library to link with. It is one of the many thread models in the field. P in pthread comes from the POSIX (Portable Operating System Interface) which is the family of IEEE operating system interface standards.

The `pthread_t` is the structure which holds the id of any POSIX compliant thread. In a modern GNU/Linux system, any newly created thread is assigned to a core until the core number is reached. This structure guarantees that every thread runs concurrently without any interference to each other. After that, new threads are assigned to the same cores again and again. These threads are scheduled implicitly by the operating system.

To create a thread in a GNU/Linux system with the C language, the following procedure can be followed.

**Step 1** Creating the thread pointer by defining a `pthread_t` object.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int main(void) {
    pthread_t tid; // Holds the thread id.
    ...
    return 0;
}
```
Although it doesn't seem like a pointer, the `pthread_t` object is actually points to a thread. In other words, it carries the id of the thread. However, without any initialization step it doesn't hold anything.

**Step 2** Initializing and starting the `pthread_t` object.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* runner(void *args) {
    ...
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid; // Holds the thread id.
    pthread_create(&tid, NULL, runner, NULL); // Create and start the thread
    ...
    return 0;
}
```

Here the thread runs the function called `runner`. This function takes one `void*` argument and returns a `void*`. This feature is very important since `void*` can pass or return anything. It's a generic type to pass and return any type. The last argument of the `pthread_create` function is the arguments passed to the thread's `runner` function. Finally, when the `runner` function finished, the `pthread_exit` function returns a `void*` which is passed as its parameter and terminates the calling thread.

**Step 3** Waiting the running threads to finish before terminating the main program.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* runner(void *args) {
    ...
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid; // Holds the thread id.
    pthread_create(&tid, NULL, runner, NULL); // Create and start the thread
    ...
    pthread_join(tid, NULL); // Wait for the tid thread to finish.
    return 0;
}
```

The following quote from the Linux manual page of the `pthread_join` perfectly clarifies how to get the return status of a terminated thread.

> If the second parameter is not `NULL`, then `pthread_join` copies the exit status of the target thread into the location pointed to by the second parameter.

**Step 4** Compile and run the multi-threaded program.

```
$ gcc threads.c -pthread -std=c99 -o threads
$ ./threads
```

Above program is run with two threads. The first one is `main` thread and the second one is the `runner` thread. The `main` thread will wait for the `runner` thread to terminate before terminating itself.

## Exercises
**Exercise 1** Write a program that consists of 2 threads. One is waiting for 10 seconds. In the mean time the main thread counts from 1 to 10.

```C
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void* waiter(void *args) {
    printf("Waiter is sleeping...");
    sleep(10);
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid;

    printf("Before thread\n");
    pthread_create(&tid, NULL, waiter, NULL);
    printf("After thread is started.");
    for (int i = 1; i < 10; i++) {
        printf("%d\n", i);
    }

    pthread_join(tid, NULL);
    printf("After thread.");

    return 0;
}
```

**Exercise 2** Write a program that prints numbers from 1 to 1000 and 1000 to 2000 simultaneously. Redirect the output of the program to a file called `output.txt`.

```C
#include <stdio.h>
#include <pthread.h>

void* runner(void *args) {
    int num;

    for (num = 1000; num <= 2000; num++) {
        printf("From runner: %d\n", num);
    }

    pthread_exit(NULL);
}

int main(void) {
    int num;
    pthread_t tid;
    pthread_create(&tid, NULL, runner, NULL);

    for (num = 1; num <= 1000; num++) {
        printf("From main: %d\n", num);
    }

    pthread_join(tid, NULL);
    return 0;
}
```

**Exercise 3** Write a program that calculates averages of two arrays simultaneously.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct CalcMod {
    int nums[1000];
    float result;
};

void* calculateAverage(void *args) {
    struct CalcMod *calcMod = (struct CalcMod*) args;
    float average = 0;

    for (int i = 0; i < 1000; i++) {
        average += calcMod->nums[i];
    }

    calcMod->result = average / 1000.0;

    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid1, tid2;
    struct CalcMod m1, m2;

    for (int i = 0; i < 1000; i++) {
        m1.nums[i] = ((i + 1000) * 108) % 550;
        m2.nums[i] = ((i + 1000) * 202) % 708;
    }

    pthread_create(&tid1, NULL, calculateAverage, &m1);
    pthread_create(&tid2, NULL, calculateAverage, &m2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Result of first array: %.2f\n", m1.result);
    printf("Result of second array: %.2f\n", m2.result);

    return 0;
}
```

**Exercise 4** Analyze and run the following program to test for race conditions.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* updater(void *args) {
    int myid;
    myid = *((int*) args);
    //free(args);
    printf("My id is %d\n", myid);

    pthread_exit(NULL);
}

int main(void) {
    pthread_t tids[2];
    int *idPtr;

    for (int i = 0; i < 2; i++) {
        //idPtr = malloc(sizeof(int));
        //*idPtr = i;
        pthread_create(&tids[i], NULL, updater, &i/*idPtr*/);
    }

    pthread_join(tids[0], NULL);
    pthread_join(tids[1], NULL);

    return 0;
}
```


**Exercise 5** Write a program that can calculate partial sums of a sequence simultaneously.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ARRAY_LENGTH 100
#define THREAD_COUNT 5

struct sum_t {
    int numGroup[20];
    int result;
};

void* calculate(void *args) {
    struct sum_t *results = (struct sum_t*) args;
    results->result = 0;

    for (int i = 0; i < 20; i++) {
        results->result += results->numGroup[i];
    }
    pthread_exit(NULL);
}

int main(void) {
    int sum = 0;
    pthread_t tids[THREAD_COUNT];
    int numbers[ARRAY_LENGTH];
    struct sum_t results[THREAD_COUNT];

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        numbers[i] = i;
    }
    for (int i = 0; i < THREAD_COUNT; i++) {
        for (int j = 0; j < 20; j++) {
            results[i].numGroup[j] = numbers[j + i * 20];
        }

        if (pthread_create(&tids[i], NULL, calculate, &results[i])) {
            fprintf(stderr, "An error occured while creating thread %d.\n", i);
        }
    }

    for (int i = 0; i < THREAD_COUNT; i++) {
        pthread_join(tids[i], NULL);
        printf("Result of %d is %d.\n", i, results[i].result);
        sum += results[i].result;
    }
    printf("And the final result is %d.\n", sum);
    return 0;
}
```
