# CMPE431 Lab Activities
## Grep Activities
1. Print the number of blank lines in the file.
```
$ grep -c '^$' file
```
2. Print the number of lines that contains upper case letters.
```
$ grep -c '[[:upper:]]' file
```
3. Find all the comments for the given C source code.
```
$ grep '^.\*' *.h
```
4. Show only the processes which ends with `sh`.
```
$ ps -ef | grep 'sh$'
```

## Find
The full command is as follows:
```
$ find / -name foo.txt -type f -print
```
The first argument is the directory where the search starts. `-name foo.txt` is the name of the file that will be searched. `-type f` specifies what kind of file we search for. There is a list of supported types in the man page of the find command. In this case it is a *file*. Final part is the `-print` option. This part is optional unless you use it in a script.

The following command searches for a file called `foo.scala` in multiple directories.
```
$ find /opt /usr /var -name foo.scala -type f
```

Case insensitive search is same as the simple search except for a little difference.
```
$ find . -iname foo
$ find . -iname foo -type d
$ find . -iname foo -type f
```

For finding files with different extensions the following commands may be used.
```
$ find . -type f \( -name "*.c" -o -name "*.sh" \)
$ find . -type f \( -name "*cache" -o -name "*xml" -o -name "*html" \)
```
The `-o` option means logical OR.

To invert the result of a search pattern the following command might be used.
```
$ find . -type f -not -name "*.html"
```

Find the files that contains a text in them.
```
$ find . -type f -name "*.java" -exec grep -l StringBuffer {} \;
```
In the above example all files that ends with `.java` extension are given to the grep command. The end of the grep command is indicated by `;`. However, to protect the `;` from the unwanted shell interference there is a preceding `\`. The `{}` is just a placeholder where the files found by `find` can go.

To find and delete files with one command the following command is very useful.
```
$ find . -type f -name "Foo*" -exec rm {} \; 
```

## Sort
Sort the current directory with respect to the file size.
```
$ ls -al | sort -n -k5
```
For more options with sort you can check the manual page.
```
$ man sort
```

## Shell Scripts
All shell scripts should start with `#!`. These symbols indicate that the script can be executed directly. After these symbols an interpreter name follows. For example `#!/bin/sh`. Now, this line is called *shebang*.

To execute a shell script. The file must have the execute permission. In shell scripts strings are quoted either single quotes or double quotes. Double quotes permit variable substitution. Single quotes not.

Shell scripts are not the only script types that supports *shebang*. The following example also shows a Python script may also use *shebang*.
```python
#!/usr/bin/env python3

print("Hello world.")
```

### Variables
Table 9.1 shows all predefined variables for a script.

To get input from the user:
```sh
#!/bin/sh
echo "Enter a phrase: \c"
read myPhrase
echo "You wrote $myPhrase"
```

## Reminder
Compile and run the following program. Record the output of the program to a file.
```C
#include <stdio.h>

int main(void) {
    int myNumber = 0;
    fprintf(stdout, "This text is written to the stdout file descriptor.\n");
    fprintf(stderr, "This is an error text written to the stderr file desc.\n");
    fscanf(stdin, "%d", &myNumber);
    printf("You entered %d\n", myNumber);
    return 0;
}
```
```
$ gcc prog.c -o prog
$ ./prog > output.txt
```

Discuss about the outputs.