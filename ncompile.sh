#!/bin/bash

fileName=$1

gcc "$fileName.c" -g -std=c99 -Werror -pthread -fopenmp -o $fileName
