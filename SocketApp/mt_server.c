#include "mt_server.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

static int get_next_available_thread_id();
static void print_error_terminate(const char *function_name);
static void* accept_connections(void *args);
static void* client_handler(void *args);

pthread_t listener_tid;
pthread_t clients_list[30];
int client_availability[30];
int client_counter = 0;
int running = 0;
int addrlen = 0;
int sockfd = -1;

void init_tcp_server(int port) {
    printf("Inside init_tcp_server.\n");

    for (int i = 0; i < 30; i++) {
        client_availability[i] = 0;
    }

    // Create the socket interface
    sockfd = socket(PF_INET, SOCK_STREAM, 0);

    if (sockfd == -1) print_error_terminate("socket");

    // Interface created, now create the binding of address
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port); // Convert host to network byte order
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    addrlen = sizeof(struct sockaddr_in);

    // Now socket is bound to a port.
    int status = bind(sockfd, (struct sockaddr*) &address, sizeof(address));
    if (status == -1) print_error_terminate("bind");
}

void start_to_serve() {
    printf("Inside start_to_serve.\n");
    // Listen is non blocking. Returns immediatelly
    int status = listen(sockfd, 10);
    if (status == -1) print_error_terminate("listen");

    running = 1;
    pthread_create(&listener_tid, NULL, accept_connections, &sockfd);
}

void destroy_tcp_server() {
    printf("Inside destroy_tcp_server.\n");
    running = 0;

    shutdown(sockfd, SHUT_RDWR);

    for (int i = 0; i < 30; i++) {
        if (client_availability[i] == 1) {
            pthread_join(clients_list[i], NULL);
        }
    }

    int status = close(sockfd);
    if (status == -1) print_error_terminate("close");
    pthread_join(listener_tid, NULL);
}

int get_next_available_thread_id() {
    printf("Inside get_next_available_thread_id.\n");
    for (int i = 0; i < 30; i++) {
        if (client_availability[i] == 0) {
            return i;
        }
    }

    return -1;
}

void print_error_terminate(const char *function_name) {
    int error_code = errno;
    fprintf(stderr, "Call to %s() failed with error code %d.\n", function_name, error_code);
    exit(-1);
}

void* accept_connections(void *args) {
    printf("Inside accept_connections.\n");

    while (running) {
        if (client_counter < 30) {
            struct sockaddr client_addr;
            int socklen = sizeof(struct sockaddr_in);
            int nsockfd = accept(sockfd, &client_addr, &socklen);
            if (nsockfd == -1) print_error_terminate("accept");
            int client_id = get_next_available_thread_id();

            struct client_info *c_info = (struct client_info*) malloc(sizeof(*c_info));

            c_info->client_id = client_id;
            c_info->sockfd = nsockfd;
            c_info->addr = client_addr;
        
            client_availability[client_id] = 1;
            pthread_create(&clients_list[client_id], NULL, client_handler, c_info);
            client_counter++;
        } else {
            fprintf(stderr, "Server is full.\n");
        }
    }

    pthread_exit(NULL);
}

void* client_handler(void *args) {
    printf("Inside client_handler.\n");
    struct client_info *c_info = (struct client_info*) args;
    char buf[50];
    char outbuf[200];

    int count = recv(c_info->sockfd, buf, 50, 0);
    strcpy(outbuf, "From client: ");
    strcat(outbuf, buf);
    strcat(outbuf, "\n");
    send(c_info->sockfd, outbuf, strlen(outbuf) + 1, 0);
    close(c_info->sockfd);
    client_availability[c_info->client_id] = 0;
    client_counter--;
    free(c_info);
    pthread_exit(NULL);
}
