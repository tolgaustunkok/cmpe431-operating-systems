#ifndef MT_SERVER_H
#define MT_SERVER_H

#include <netdb.h>

struct client_info {
    int client_id;
    int sockfd;
    struct sockaddr addr;
};

void init_tcp_server(int port);
void start_to_serve();
void destroy_tcp_server();


#endif