#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

static void print_error_terminate(const char *function_name);
int run = 0;

void signal_handler(int signum) {
    printf("\nSIGINT catched.\nStarting termination process...\n");
    run = 0;
}

int init_tcp_server(int port) {
    int status;
    struct sockaddr_in addr;
    int srv_sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    signal(SIGINT, signal_handler);
    if (srv_sockfd == -1) print_error_terminate("socket");

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    status = bind(srv_sockfd, (struct sockaddr*) &addr, sizeof(addr));
    if (status == -1) print_error_terminate("bind");

    return srv_sockfd;
}

void start_listen(int srv_sockfd) {
    int status = listen(srv_sockfd, 5);
    if (status == -1) print_error_terminate("listen");
}

void accept_connections(int srv_sockfd) {
    run = 1;

    while (run) {
        char buffer[32] = { 0 };
        int status;
        struct sockaddr client_addr;
        int socklen = sizeof(struct sockaddr_in);
        int client_sockfd = accept(srv_sockfd, &client_addr, &socklen);

        status = recv(client_sockfd, buffer, 32, 0);
        if (status == -1) print_error_terminate("recv");

        status = send(client_sockfd, buffer, strlen(buffer) + 1, 0);
        if (status == -1) print_error_terminate("send");

        close(client_sockfd);
    }

    close(srv_sockfd);
}

void print_error_terminate(const char *function_name) {
    int error_code = errno;
    fprintf(stderr, "Call to %s() failed with error code %d.\n", function_name, error_code);
    exit(-1);
}
