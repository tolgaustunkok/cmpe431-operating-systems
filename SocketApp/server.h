#ifndef SERVER_H
#define SERVER_H

int init_tcp_server(int port);
void start_listen(int srv_sockfd);
void accept_connections(int srv_sockfd);

#endif