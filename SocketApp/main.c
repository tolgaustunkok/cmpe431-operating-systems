#include "server.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int sockfd = init_tcp_server(8080);
    start_listen(sockfd);
    accept_connections(sockfd);

    return 0;
}