#! /bin/bash

function displayTextProcCommands() {
    PS3="Enter your choice: "
    local options=("Search a file for a pattern" "Count lines, words, and characters in specified files" "Display line differences btw two files" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                read -p "File name: " fileName
                read -p "Pattern: " pattern
                grep $pattern $fileName
                ;;
            "${options[1]}")
                read -p "File name: " fileName
                wc -lwm $fileName
                ;;
            "${options[2]}")
                read -p "Name of the 1st file: " fileName1
                read -p "Name of the 2nd file: " fileName2
                diff $fileName1 $fileName2
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY from TP."
                ;;
        esac
    done
}