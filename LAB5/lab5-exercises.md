# LAB5 - Exercises
## Case Study
Build a menu system for a novice Linux user. There should be at least 4 menus consist of submenus and menus. The requirements are following.

1. User can see contents of a file.
2. User can remove a file.
3. User can copy a file.
4. User can list a directory.
5. User can display size of a file.
6. User can search a file for a pattern.
7. User can display line count, word count, and character count of a file.
8. User can display line differences between any two files.
9. User can display current date and time.
10. User can display current disk usage.
11. User can display details of current environment.
12. User can display a snapshot of process status information.

These requirements may be categorized as the following table.

File & Directory Management | Text Processing | System Status
--------------------------- | --------------- | -------------
See contents of a file | Search a file for a pattern | Display current date and time
Remove a file | Display line cnt, word cnt, char cnt | Current disk usage
Copy a file | Display line differences | Details of current environment
List a directory | - | Display process status info
Display size of a file | - | -

The program must consist of multiple shell scripts. **DO NOT PUT ALL PROGRAM INTO A SINGLE SCRIPT FILE**. All menus must also have an option to return the parent menu. Parent menu also must have an option to exit from the program.

Apart from above, the program must also support some command line options such as `file`, `text`, `status`. For example, if the script run with `file` option, the file management menu should come up.

### Sample Run

```
$ ./novice_menu.sh
1) File and Directory Management Commands
2) Text Processing Commands
3) System Status Commands
4) Exit
Enter your choice: 1
1) Display the contents of a file  4) List a directory
2) Remove a file                   5) Size of a file
3) Copy a file                     6) Quit -- Return to main menu
Enter your choice: 1
File name: test.txt
This is a text file.
Second line
Third line
Forth line
Enter your choice: 5
File name: 3
stat: cannot stat '3': No such file or directory
Enter your choice: 6
Returning to main menu.
Enter your choice: 3
1) Display the current date and time   3) Display process status information
2) Current disk usage                  4) Quit -- Return to main menu
Enter your choice: 1
Cts Kas  3 14:17:06 +03 2018
Enter your choice: 4
Quitting.
Enter your choice: 4
Quitting.

$ ./novice_menu.sh status
1) Display the current date and time   3) Display process status information
2) Current disk usage                  4) Quit -- Return to main menu
Enter your choice: 4
Quitting.
1) File and Directory Management Commands
2) Text Processing Commands
3) System Status Commands
4) Exit
Enter your choice: 4
Quitting.
```

### Answer of Case Study (To be deleted)
Contents of `novice_menu.sh`.

```sh
#! /bin/bash

source main_menu.sh
source FDMCommands.sh
source TextProcCommands.sh
source SysStatCommands.sh

if [ $# -eq 1 ]
then
    case $1 in
        "file")
            displayFDMCommands
            ;;
        "text")
            displayTextProcCommands
            ;;
        "status")
            displaySysStatCommands
            ;;
        *)
            echo "Usage: $0 [file|text|status]"
            exit 0
            ;;
    esac
elif [ $# -gt 1 ]
then
    echo "Usage: $0 [file|text|status]"
    exit 0
fi

displayMainMenu
```

Contents of `main_menu.sh`.

```sh
#! /bin/bash

function displayMainMenu() {
    PS3="Enter your choice: "
    local options=("File and Directory Management Commands" "Text Processing Commands" "System Status Commands" "Exit")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                displayFDMCommands
                ;;
            "${options[1]}")
                displayTextProcCommands
                ;;
            "${options[2]}")
                displaySysStatCommands
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}
```

Contents of `FDMCommands.sh`.

```sh
#! /bin/bash

function displayFDMCommands() {
    PS3="Enter your choice: "
    local options=("Display the contents of a file" "Remove a file" "Copy a file" "List a directory" "Size of a file" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                read -p "File name: " fileName
                cat $fileName
                ;;
            "${options[1]}")
                read -p "File name: " fileName
                rm -iv $fileName
                ;;
            "${options[2]}")
                read -p "Source file name: " sourceFile
                read -p "Destination: " destionation
                cp -v $sourceFile $destionation
                ;;
            "${options[3]}")
                read -p "Directory name: " dirName
                ls -l $dirName
                ;;
            "${options[4]}")
                read -p "File name: " fileName
                stat --printf="Total size is %s bytes.\n" $fileName
                ;;
            "${options[5]}")
                echo "Returning to main menu."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}
```

Contents of `TextProcCommands.sh`.

```sh
#! /bin/bash

function displayTextProcCommands() {
    PS3="Enter your choice: "
    local options=("Search a file for a pattern" "Count lines, words, and characters in specified files" "Display line differences btw two files" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                read -p "File name: " fileName
                read -p "Pattern: " pattern
                grep $pattern $fileName
                ;;
            "${options[1]}")
                read -p "File name: " fileName
                wc -lwm $fileName
                ;;
            "${options[2]}")
                read -p "Name of the 1st file: " fileName1
                read -p "Name of the 2nd file: " fileName2
                diff $fileName1 $fileName2
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY from TP."
                ;;
        esac
    done
}
```

Contents of `SysStatCommands.sh`.

```sh
#! /bin/bash

function displaySysStatCommands() {
    PS3="Enter your choice: "
    local options=("Display the current date and time" "Current disk usage" "Display process status information" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                date
                ;;
            "${options[1]}")
                df -h
                ;;
            "${options[2]}")
                ps -ef
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}
```

**Exercise 1** Write a command to move all text files from multiple directories whose names are starts with dir (e.g. dir1/, dir2, dir3/, etc.) recursively.

```
$ find ./dir* -type f -name "*.txt" -exec mv -iv {} ./ \;
```

**Exercise 2** Write a script that merges all text files in a directory. The directory name must given as an argument to the script.

```sh
#! /bin/bash

for textFile in *.txt
do
    cat $textFile >> result.txt
done
```

**Exercise 3** Write a script to create an output file containing a customized loan amortization table. Your program will prompt the student to enter the amount borrowed (the principal), the annual interest rate, and the number of payments (term). To calculate the payment, it will use the following formula.

$$payment=\frac{iP}{1-(1+i)^{-n}}$$

where  
&nbsp;&nbsp;&nbsp;&nbsp;$P$ = principal (the amount he borrowed)  
&nbsp;&nbsp;&nbsp;&nbsp;$i$ = monthly interest rate ($\frac{1}{12}$ of the annual rate)  
&nbsp;&nbsp;&nbsp;&nbsp;$n$ = total number of payments

The program will write to the output file n lines showing how the debt is paid off. Each month part of the payment is the monthly interest on the principal balance, and the rest is applied to the principal. Here is a sample table for a $1000 loan borrowed at a 9% annual interest rate and paid back over six months.

### Sample Table
Principal           $1000  
Annual interest     9.0%  
Payment             $171.07  
Term                6 months

Payment Balance | Interest | Principal | Principal
--------------- | -------- | --------- | ---------
1 | 7.50 | 163.57 | 836.43
2 | 6.27 | 164.80 | 671.63
3 | 5.04 | 166.03 | 505.60
4 | 3.79 | 167.28 | 338.32
5 | 2.54 | 168.53 | 169.79
6 | 1.27 | 169.79 | 0.00
Final payment | $171.06

*Source*: Problem Solving and Program Design in C, 7th Edition, Hanly and Koffman

```sh
#! /bin/bash

read -p 'Enter the amount borrowed: ' borrowed
read -p 'Enter the annual interest rate: ' annualInterestRate
read -p 'Enter the number of payments: ' term

i=`echo "(1 / 12) * $annualInterestRate" | bc -l`
payment=`echo "($i * $borrowed) / (1 - (1 + $i)^-$term)" | bc -l`
```