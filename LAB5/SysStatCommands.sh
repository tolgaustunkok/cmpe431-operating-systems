#! /bin/bash

function displaySysStatCommands() {
    PS3="Enter your choice: "
    local options=("Display the current date and time" "Current disk usage" "Display process status information" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                date
                ;;
            "${options[1]}")
                df -h
                ;;
            "${options[2]}")
                ps -ef
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}