#! /bin/bash

function displayFDMCommands() {
    PS3="Enter your choice: "
    local options=("Display the contents of a file" "Remove a file" "Copy a file" "List a directory" "Size of a file" "Quit -- Return to main menu")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                read -p "File name: " fileName
                cat $fileName
                ;;
            "${options[1]}")
                read -p "File name: " fileName
                rm -iv $fileName
                ;;
            "${options[2]}")
                read -p "Source file name: " sourceFile
                read -p "Destination: " destionation
                cp -v $sourceFile $destionation
                ;;
            "${options[3]}")
                read -p "Directory name: " dirName
                ls -l $dirName
                ;;
            "${options[4]}")
                read -p "File name: " fileName
                stat --printf="Total size is %s bytes.\n" $fileName
                ;;
            "${options[5]}")
                echo "Returning to main menu."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}