#! /bin/bash

function displayMainMenu() {
    PS3="Enter your choice: "
    local options=("File and Directory Management Commands" "Text Processing Commands" "System Status Commands" "Exit")

    select opt in "${options[@]}"
    do
        case $opt in
            "${options[0]}")
                displayFDMCommands
                ;;
            "${options[1]}")
                displayTextProcCommands
                ;;
            "${options[2]}")
                displaySysStatCommands
                ;;
            "${options[3]}")
                echo "Quitting."
                break
                ;;
            *)
                echo "Invalid option $REPLY."
                ;;
        esac
    done
}