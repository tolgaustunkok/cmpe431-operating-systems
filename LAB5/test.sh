#! /bin/bash

source main_menu.sh
source FDMCommands.sh
source TextProcCommands.sh
source SysStatCommands.sh

if [ $# -eq 1 ]
then
    case $1 in
        "file")
            displayFDMCommands
            ;;
        "text")
            displayTextProcCommands
            ;;
        "status")
            displaySysStatCommands
            ;;
        *)
            echo "Usage: $0 [file|text|status]"
            exit 0
            ;;
    esac
elif [ $# -gt 1 ]
then
    echo "Usage: $0 [file|text|status]"
    exit 0
fi

displayMainMenu