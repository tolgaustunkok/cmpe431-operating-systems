# LAB4 - Scripts
**Every shell run its own bash environment.** To run a script first thing to do is to make sure that the script has executable permission. To make a script executable:
```
$ chmod u+x test.sh
$ ./test.sh
```

## Shebang / Hashbang Line
This line specifies the which interpreter should be used to interpret the script.
```sh
#! /bin/bash
#! /bin/sh
```

## Variables and Comments
Comments are line of scripts that are not interpreted by the interpreter. In shell the comments are indicated by hash symbol `#`.
```sh
#! /bin/bash

# this is a comment
echo "Hello world" # This is another comment.
```

There are two types of variables in a Linux system. One is system variables and the other one is user defined variables. If a variable consist of capital letters, then most likely it is a system variable.

```sh
#! /bin/bash

echo "The shell name is $BASH"
echo "The shell version is $BASH_VERSION"
echo "Our home directory is $HOME"
echo "Current working directory is $PWD"

name="Tolga"

echo $name
echo $name_wq

echo "The name is $name"
```

## Reading User Inputs
Reading user input can be done by the `read` command.

```sh
#! /bin/bash

echo "Enter name: "
read name
echo "Your name is $name"
```

```sh
#! /bin/bash

echo "Enter multiple names: "
read name1 name2 name3
echo "Names: $name1, $name2, $name3"
```

To get the input in the same line with the prompt:

```sh
#! /bin/bash

read -p 'username: ' user_name
read -sp 'password: ' user_pass
echo "Username is $user_var"
echo "Password is $user_pass"
```

Look for the man page of the `read` to get information about `-s` option. To get the an array of inputs from the user, the `-a` option can be used with `read` command.

```sh
#! /bin/bash

echo "Enter names: "
read -a names
echo "Names: ${names[0]}, ${names[1]}, ${names[2]}"
```

If the `read` command is called without any arguments than the input of the command can be reached from the `$REPLY` variable.

```sh
#! /bin/bash

echo "Enter name: "
read
echo "Name is $REPLY"
```

## Passing Arguments to a Bash Script
The passed arguments to a bash script are stored in previously defined variables `$1`, `$2`, `$3`, etc. In addition to the arguments, the `$0` variable holds the script / command name.

```sh
#! /bin/bash

# The first three arguments
echo $1 $2 $3

echo "The command name is $0"
```

To access the arguments list as an array, we can store the arguments to a user defined array variable.

```sh
#! /bin/bash

args=("$@")
echo ${args[1]} ${args[2]}
echo $@
```

To print the number of arguments:

```sh
#! /bin/bash

echo "Number of arguments is $#"
```

## If Statement

```sh
#! /bin/bash

if [ condition ]; then
    statement
fi

#----------------------------

if [ condition ]
then
    statement
fi
```

### Integer Comparison Operators
* `-eq` : is equal to
* `-ne` : is not equal to
* `-gt` : is greater than
* `-ge` : is greater than or equal to
* `-lt` : is less than
* `-le` : is less than or equal to
* `<` : is less than (within double parentheses)
* `<=` : is less than or equal to (within double parentheses)
* `>` : is greater than (within double parentheses)
* `>=` : is greater than or equal to (within double parentheses)

For more information source: http://tldp.org/LDP/abs/html/comparison-ops.html

```sh
#! /bin/bash

count=10

if [ $count -eq 9 ]
then
    echo "Condition is true"
fi

if (($count > 9))
then
    echo "Condition is true"
fi
```

String comparison examples:

```sh
#! /bin/bash

word="abc"

if [ $word == "abc" ] # single equal sign is also valid
then
    echo "Condition is true"
fi
```

```sh
#! /bin/bash

word=a

if [[ $word < "b" ]]
then
    echo "Condition is true"
else
    echo "Condition is false"
fi
```

```sh
#! /bin/bash

word=a

if [[ $word == "b" ]]
then
    echo "Word is equal to b"
elif [[ $word == "a" ]]
then
    echo "Word is equalt to a"
else
    echo "Word is not equal to a or b"
fi
```

## File Testing
Dealing with files is a very important part of scripting.

```sh
#! /bin/bash

echo -e "Enter the name of the file: \c"
read fileName

# Check existense of a file
if [ -e $fileName ]
then
    echo "$fileName found."
else
    echo "$fileName not found"
fi

# Check for regular file: -f
# Check for directories: -d
```

Look man page of echo for `-e` flag.

**Example:** Check if a file exists and writable. If so, then write something in it.

```sh
#! /bin/bash

echo -e "Enter the name of the file: \c"
read fileName

if [ -f $fileName ]
then
    if [ -w $fileName ]
    then
        echo "Write some text. To quit press CTRL-D."
        cat >> $fileName
    else
        echo "You have not the write permission."
    fi
else
    echo "$fileName does not exist."
fi
```

**Example:** Write a script that checks if age is between 18 and 30.

```sh
#! /bin/bash

age=25

if [ $age -gt 18 ] && [ $age -lt 30 ]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi

#--------------------------------------

if [[ $age -gt 18 && $age -lt 30 ]]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi

#--------------------------------------

if [ $age -gt 18 -a $age -lt 30 ]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi
```

The same things are valid for or `||` operator.

## Arithmetic Operations
These arithmetic operations are for integer numbers.

**Caution:** The syntax and whitespaces are very important.

**Exercise:** Implement a simple calculator. Write a script that takes three arguments. The first two are numbers and the last one is the operator.

```sh
#! /bin/bash

if [ $# -ne 3 ]
then
    echo "Enter three arguments."
    exit 0
fi

num1=$1
num2=$2
operator=$3

if [[ $operator == "+" ]]
then
    echo "The result is $(( num1 + num2 ))"
elif [[ $operator == "-" ]]
then
    echo "The result is $(( num1 - num2 ))"
elif [[ $operator == "*" ]]
then
    echo "The result is $(( num1 * num2 ))"
elif [[ $operator == "/" ]]
then
    echo "The result is $(( num1 / num2 ))"
elif [[ $operator == "%" ]]
then
    echo "The result is $(( num1 % num2 ))"
else
    echo "Operator not found."
fi
```

Example run:
```
$ ./test.sh 10 2 \&
Operator not found.

$ ./test.sh 10 2 +
The result is 12

$ ./test.sh 10 2 \*
The result is 20
```

Instead of double parathesis the following expression might be used: `echo $(expr $num1 + $num2)`

## The Case Statement
The analog of the switch statement.

```sh
#! /bin/bash

# Patterns are regular expressions.
case expression in
    pattern1 )
        statements ;;
    pattern2 )
        statements ;;
    ...
esac
```

**Exercise:** Write the previous exercise with case statement.
```sh
#! /bin/bash

if [ $# -ne 3 ]
then
    echo "Enter three arguments."
    exit 0
fi

num1=$1
num2=$2
operator=$3

case $operator in
    "-" )
        echo "The result is $(expr $num1 - $num2)" ;;
    "+" )
        echo "The result is $(expr $num1 + $num2)" ;;
    "*" )
        echo "The result is $(expr $num1 \* $num2)" ;;
    * )
        echo "Unknown operator." ;;
esac
```

**Exercise:** Write a script that can decide if the given string starts with capital letters.

```sh
#! /bin/bash

echo -e "Enter a string: \c"
read string

case $string in
    [A-Z]* )
        echo "String starts with capital letter." ;;
    * )
        echo "String does not start with capital letter." ;;
esac
```

## Array Variables

```sh
#! /bin/bash

os=('ubuntu' 'windows' 'manjaro' 'FreeBSD')
os[4]='mac'

unset os[1] # Remove the windows from the array

echo "${os[@]}" # Print all elements
echo "${os[0]}"
echo "${!os[@]}" # Print the indices of the array
echo "${#os[@]}" # Print length of the array
```

## While Loops

```sh
#! /bin/bash

while [ condition ]
do
    command1
    command2
    ...
done
```

**Exercise:** Write a script that prints numbers from 1 to 10.

```sh
#! /bin/bash

n=1

while [ $n -le 10 ]
do
    echo "$n"
    # n=$(expr $n + 1)
    # n=$(( n + 1 ))
    (( n++ ))
    sleep 1
done

#-----------------------

while (( $n <= 10))
do
    echo $n
    (( n++ ))
done
```

**Exercise:** Read the file with while loop.
```sh
while read line
do
    echo $line
done < test3.sh

#------------------

cat test3.sh | while read line
do
    echo $line
done
```

## Until Loops
Almost same as while loops with a slight difference. If the condition is false, then the commands between do and done are executed.

```sh
#! /bin/bash

until [ condition ]
do
    command1
    command2
    ...
    commandN
done
```

**Exercise:** Print 1 to 10 to the shell.

```sh
#! /bin/bash

n=1

until [ $n -gt 10 ]
do
    echo $n
    (( n++ ))
done
```

## For Loop

```sh
#! /bin/bash

for myVar in 1 2 3 4 5
do
    echo $myVar
done

#----------------------------

for myVar in {1..10..2} # {START..END..INCREMENT}
do
    echo $myVar
done

#----------------------------

for (( i=0; i<5; i++ ))
do
    echo $i
done
```

For can run several commands in sequence.

```sh
#! /bin/bash

for cmd in ls pwd date
do
    echo "--------------$cmd--------------"
    $cmd
done
```

**Exercise:** Print all directories in the folder.

```sh
#! /bin/bash

for item in *
do
    if [ -d "$item" ]
    then
        echo "$item"
    fi
done
```

## Select Loop
To create text based menus, the select loop is the command you gonna use.

```sh
#! /bin/bash

select varName in list
do
    command1
    command2
    ...
    commandN
done
```

**Exercise:** Print 4 options and print the selected one.

```sh
#! /bin/bash

select name in mark john tom ben
do
    echo "$name selected."
done
```

Generally used with case statements.

```sh
#! /bin/bash

select name in mark john tom ben
do
    case $name in
        mark)
            echo "Mark selected." ;;
        *)
            echo "Error: please provide number between 1..4"
    esac
done
```

## Functions

```sh
#! /bin/bash

function name() {
    command1
    command2
    ...
    commandN
}

name() {

}
```

**Exercise:** Write a script that prints hello world with functions.

```sh
#! /bin/bash

function hello() {
    echo "Hello"
}

quit() {
    exit 0
}

function print() {
    echo $1 # Print the first argument
}

# quit
print Hello
hello
```

## Local Variables
All variable in a script are global variables. So, how to create a local variable.

```sh
#! /bin/bash

function print() {
    local name=$1
    echo "The name is $name"
}

name="Tom"
echo "The name is $name : Before"
print Max
echo "The name is $name : After"
```

**Exercise:** Check wheather a file exist or not with a user defined parameter. The parameter is the name of the file.

```sh
#! /bin/bash

function usage() {
    echo "You need to provide an argument : "
    echo "usage : $0 file_name"
}

function isFileExist() {
    local file="$1"
    [[ -f "$file" ]] && return 0 || return 1
}

[[ $# -eq 0 ]] && usage

if ( isFileExist "$1" )
then
    echo "File found"
else
    echo "File not found"
fi
```

Notice that 0 is true and 1 is false.

## Constants
Constants are made with `readonly` command.

```sh
#! /bin/bash

var=30
readonly var

var=50
echo "var => $var"

function hello() {
    echo "Hello world"
}

readonly -f hello

function hello() {
    echo "Hello world from constant"
}

readonly
```

## Signals and Traps
If you press CTRL-C while a script is running, then you sent an interrupt signal to the running process. The interrupt signal is also known as *SIGINT*.

```sh
#! /bin/bash

echo "PID is $$"
while (( COUNT < 10 ))
do
    sleep 10
    (( COUNT++ ))
    echo $COUNT
done
exit 0
```

CTRL-Z signal called suspend signal (a.k.a. SIGSTOP). This signal just pauses the current state of the process. Open the man page of the signals.

```
$ man 7 signal
```

Traps can be used to give responses to signals. For example:

```sh
#! /bin/bash

trap "echo Exit command is detected" 0
echo "Hello world"
exit 0
```

```sh
#! /bin/bash

trap "echo Interrupt signal is detected." SIGINT
echo "PID is $$"
while (( COUNT < 10 ))
do
    sleep 10
    (( COUNT++ ))
    echo $COUNT
done
exit 0
```