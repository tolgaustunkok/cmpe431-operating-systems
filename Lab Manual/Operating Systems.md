# Operating Systems Lab Notes

<!-- TOC -->

- [Operating Systems Lab Notes](#operating-systems-lab-notes)
  - [0 About the Document](#0-about-the-document)
    - [0.1 Authors](#01-authors)
    - [0.2 Revisions](#02-revisions)
    - [0.3 Todo List](#03-todo-list)
    - [0.4 License](#04-license)
  - [1 Introduction to GNU/Linux](#1-introduction-to-gnulinux)
    - [1.1 Directory Navigation and Control](#11-directory-navigation-and-control)
      - [1.1.1 cd - Change Directory](#111-cd---change-directory)
      - [1.1.2 ls - List Directory](#112-ls---list-directory)
      - [1.1.3 mkdir - Make Directory](#113-mkdir---make-directory)
      - [1.1.4 pwd - Print Working Directory](#114-pwd---print-working-directory)
      - [1.1.5 rmdir - Remove Directory](#115-rmdir---remove-directory)
    - [1.2 File Maintenance Commands](#12-file-maintenance-commands)
      - [1.2.1 chgrp - Change Group](#121-chgrp---change-group)
      - [1.2.2 chmod - Change Mode](#122-chmod---change-mode)
      - [1.2.3 chown - Change Ownership](#123-chown---change-ownership)
      - [1.2.4 cp - Copy](#124-cp---copy)
      - [1.2.5 mv - Move](#125-mv---move)
      - [1.2.6 rm - Remove](#126-rm---remove)
    - [1.3 Display Commands](#13-display-commands)
      - [1.3.1 cat - Concatenate](#131-cat---concatenate)
      - [1.3.2 echo - Echo](#132-echo---echo)
      - [1.3.3 head - Display Head of a File](#133-head---display-head-of-a-file)
      - [1.3.4 less - Page Through a File](#134-less---page-through-a-file)
      - [1.3.5 tail - Display Tail of a File](#135-tail---display-tail-of-a-file)
    - [1.4 System Resources Commands](#14-system-resources-commands)
      - [1.4.1 df - Disk Free](#141-df---disk-free)
      - [1.4.2 kill - Kill a Process](#142-kill---kill-a-process)
      - [1.4.3 ps - Processes](#143-ps---processes)
  - [2 Special Linux Features](#2-special-linux-features)
    - [2.1 File Descriptors](#21-file-descriptors)
    - [2.2 File Redirection](#22-file-redirection)
    - [2.3 Other Special Command Symbols](#23-other-special-command-symbols)
    - [2.4 Wild Cards](#24-wild-cards)
  - [3 Text Processing](#3-text-processing)
    - [3.1 Regular Expression Syntax](#31-regular-expression-syntax)
    - [3.2 Text Processing Commands](#32-text-processing-commands)
      - [3.2.1 grep - g/re/p](#321-grep---grep)
      - [3.2.2 sed - Stream Editor](#322-sed---stream-editor)
      - [3.2.3 awk - Aho, Weinberger, Kernighan](#323-awk---aho-weinberger-kernighan)
  - [4 Other Useful Commands](#4-other-useful-commands)
    - [4.1 Working With Files](#41-working-with-files)
      - [4.1.1 cut - Cut Specified Fields](#411-cut---cut-specified-fields)
      - [4.1.2 find - Find Files Matching a Type or Pattern](#412-find---find-files-matching-a-type-or-pattern)
      - [4.1.3 sort - Sort The Lines of a File](#413-sort---sort-the-lines-of-a-file)
  - [5 Shell Scripts](#5-shell-scripts)
    - [5.1 Shebang / Hashbang Line](#51-shebang--hashbang-line)
    - [5.2 Variables and Comments](#52-variables-and-comments)
    - [5.3 Reading User Inputs](#53-reading-user-inputs)
    - [5.4 Passing Arguments to a Bash Script](#54-passing-arguments-to-a-bash-script)
    - [5.5 If Statement](#55-if-statement)
      - [5.5.1 Integer Comparison Operators](#551-integer-comparison-operators)
    - [5.6 File Testing](#56-file-testing)
    - [5.7 Arithmetic Operations](#57-arithmetic-operations)
      - [5.7.1 Integer Arithmetic](#571-integer-arithmetic)
      - [5.7.2 Floating Point Arithmetic](#572-floating-point-arithmetic)
    - [5.8 The Case Statement](#58-the-case-statement)
    - [5.9 Array Variables](#59-array-variables)
    - [5.10 While Loops](#510-while-loops)
    - [5.11 Until Loops](#511-until-loops)
    - [5.12 For Loop](#512-for-loop)
    - [5.13 Select Loop](#513-select-loop)
    - [5.14 Functions](#514-functions)
    - [5.15. Local Variables](#515-local-variables)
    - [5.16 Constants](#516-constants)
    - [5.17 Signals and Traps](#517-signals-and-traps)
  - [6 A Case Study](#6-a-case-study)
  - [7 Synchronization](#7-synchronization)
    - [7.1 Threads in GNU/Linux](#71-threads-in-gnulinux)
    - [7.2 POSIX Threads (pthread)](#72-posix-threads-pthread)
      - [7.2.1 Exercises](#721-exercises)
    - [7.3 Mutex Synchronization](#73-mutex-synchronization)
      - [7.3.1 Deadlocks via Mutexes](#731-deadlocks-via-mutexes)
    - [7.4 Semaphore Synchronization](#74-semaphore-synchronization)
      - [7.4.1 Readers - Writers Problem](#741-readers---writers-problem)
      - [7.4.2 Dining Philosophers Problem](#742-dining-philosophers-problem)
  - [8 Communication Over Sockets in C](#8-communication-over-sockets-in-c)
    - [8.1. A Single Client-Server Communication Example](#81-a-single-client-server-communication-example)
  - [9 Parallel Computing With OpenMP](#9-parallel-computing-with-openmp)
    - [9.1 Getting Started With OpenMP](#91-getting-started-with-openmp)
    - [9.2 The Core Features of OpenMP](#92-the-core-features-of-openmp)
    - [9.3 Working With OpenMP](#93-working-with-openmp)
    - [9.4 Advenced OpenMP](#94-advenced-openmp)

<!-- /TOC -->

## 0 About the Document
This document is mainly created to gather laboratory notes of CMPE431 - Operating Systems course offered by Department of Computer Engineering, Atılım University. This version of the document is free to distribute.

The contents of the document generally follows the CMPE431 lecture content without much divergence. Some topics such as OpenMP is added as an extra content.

The document is under ongoing development. The underlying version control tool is Git. The most recent version of the document can be found on [BitBucket](https://bitbucket.org/tolgaustunkok/cmpe431-operating-systems/src/master/).

### 0.1 Authors
* **Res. Asst. Tolga Üstünkök** - *M. Sc. Student at Department of Software Engineering, Atılım University*

### 0.2 Revisions
[SemVer](https://semver.org) is used for document versioning.

* 19.04.2019 - Section 7 Exercise 4 is updated - v0.6.3
* 01.04.2019 - License information is added - v0.6.2
* 01.04.2019 - About the Document part is added - v0.6.1
* 03.12.2018 - OpenMP section is added - v0.6.0
* 02.12.2018 - Removed The Bounded-Buffer Problem section - v0.5.2
* 02.12.2018 - Added fully functional socket communication example - v0.5.1
* 02.12.2018 - Added socket programming section - v0.5.0
* 28.11.2018 - Added Readers-Writers Problem and Dining Philosophers Problem with solutions - v0.4.1
* 26.11.2018 - Added semaphores and related exercises (The Bounded-Buffer Problem) - v0.4.0
* 21.11.2018 - Added some explanations to the loops and made some spacing corrections - v0.3.1
* 20.11.2018 - Mutexes are added to the synchronization section - v0.3.0
* 16.11.2018 - Some explanations are added in the interval of shell scripts and arrays - v0.2.0
* 15.11.2018 - Initial document creation - v0.1.0

### 0.3 Todo List
1. *sed* examples will be added.
2. *awk* examples will be added.
3. Floating point arithmetic will be added.
4. Shared memory examples will be added.

### 0.4 License
Copyright (C) 2019 Tolga Üstünkök

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## 1 Introduction to GNU/Linux
The most important command for a standard Linux user is the `man` command. `man` command displays the manual page of the specified command.

```
man [chapter] word # Display the manual page of the word
man -k keyword # Display the list of manual pages that contains keyword
```

Note that all characters that follow `#` symbol are all comments.

### 1.1 Directory Navigation and Control

#### 1.1.1 cd - Change Directory
In a file system, the most performed operation is the changing the current directory to another one. The command that is related with this operation is the `cd` command. The general structure of the command is as follows.

```
cd [directory]
```

If you do not provide the directory, the command will take you to your home directory. The tilda `~` also performs the same operation if provided as an argument.

#### 1.1.2 ls - List Directory
To understand where you are at and discover your surroundings, your command is the `ls`. It lightenes up the supplied environment. The general structure of the command is as follows.

```
ls [options] [directory or file]
```

It provides information such as the permissions of the file/directory, size, etc. depending on the provided arguments. All applicable arguments and their related parameters can be found in the manual page.

#### 1.1.3 mkdir - Make Directory
If you want to create a directory or multiple directories, your command is the `mkdir`. The general structure of the command is given as follows.

```
mkdir [options] directory
```

You can find information about its options in the man page of the command as with all other commands.

#### 1.1.4 pwd - Print Working Directory
It may be the most basic command in a Linux shell. It prints the working (current) directory to the `stdout`. Some shells do not show you your current location. With these shells `pwd` might be very useful. The general structure is given as follows.

```
pwd
```

#### 1.1.5 rmdir - Remove Directory
As the name suggest, it is the complement of the `mkdir` command. In other words, you can delete a directory with its contents via `rmdir` command. The general structure is given as follows.

```
rmdir [options] directory
```

### 1.2 File Maintenance Commands
These commands are the basic operations that can be performed on a file. For example; create, copy, remove, change permissions, etc.

#### 1.2.1 chgrp - Change Group
This command changes the group of a file. With some options this command can be applied to a directory recursively. You can find more information about arguments in manual page as always. The basic syntax is as follows.

```
chgrp [options] group file
```

#### 1.2.2 chmod - Change Mode
`chmod` is a command and a system call that can change the access permissions of a file. `chmod` is also support numerous arguments as of `chgrp`. The basic syntax is as follows.

```
chmod [options] file
```

#### 1.2.3 chown - Change Ownership
`chown` command is used to change the ownership of the specified file. The basic syntax is as follows.

```
chown [options] owner file
```

#### 1.2.4 cp - Copy
This commands copies a file to a location or overrides the target file if exists. The syntax and order of arguments are given as follows.

```
cp [options] source_file destination
```

More information can be found in the manual page of the `cp` such as multiple file copying, etc.

#### 1.2.5 mv - Move
The `mv` command is very similar to the `cp` command. The order of arguments are the same as `cp`. The difference is `mv` command deletes the source file after copying it to the specified location.

```
mv [options] source_file destionation
```

`mv` command can also be used as a renaming command because of its deleting the source file nature. So, if you want to rename a file or directory, you can use the `mv` command.

#### 1.2.6 rm - Remove
`rm` command can remove anything from a file or group of files to a directory or group of directories with the proper arguments. It should be used with care to accidently delete any important files. The general syntax is as follows.

```
rm [options] file
```

There some important options that prevent you to make accidental deletions. They are `-i`, `f`. The `-i` option prompts you to every time before deleting a file. However, the `-f` option is dangerous because it means force the deletion. So, if you use this option, no matter what is going on with the target file, it is going to be deleted by `rm` command. **Use with care!!!**

### 1.3 Display Commands
There are numerous ways to display a file in a Linux system. This section shows only some of them.

#### 1.3.1 cat - Concatenate
`cat` is one of the most used command in a Linux system. It prints the contents of a file to `stdout` which may be redirected to somewhere else. The simplest syntax is given as follows.

```
cat [options] file
```

There will be lots of examples of `cat` in later sections.

#### 1.3.2 echo - Echo
`echo` command prints the given string to the `stdout`. It is very useful in certain situations as well as giving some information to the user. The basic syntax is given as follows.

```
echo [text string]
```

There are lots of very useful options to be given to the `echo` command. You can always check the man page of the `echo`.

#### 1.3.3 head - Display Head of a File
There will be some situations that you have a very large text file that only contains some important information in its first few lines. And these lines may be updated regularly. So, how can you analyze only those lines? The answer is the `head` command. You can provide how many lines do you want to see from the top of a file. The general sytax is as follows.

```
head [-number] file
```

#### 1.3.4 less - Page Through a File
`less` is a program to inspect files. It can work easily with very large files without filling the memory of the computer. It also supports regular expression search. The general syntax is as follows.

```
less [options] file
```

#### 1.3.5 tail - Display Tail of a File
`tail` is complement of the `head` command. It supports exactly same operations with the `head` command. The basic syntax is as follows.

```
tail [options] file
```

### 1.4 System Resources Commands
These are the commands to report or manage system resources.

#### 1.4.1 df - Disk Free
`df` command reports the disk usage for all partitions except for MBR (Master Boot Record). The basic syntax is as follows.

```
df [options] [resource]
```

#### 1.4.2 kill - Kill a Process
`kill` commands sends the specified signal to the specified process. The list of signals can be seen in the man page of both `kill` command and man page chapter 7 of the `signal`. The basic syntax is given as follows.

```
kill [options] [-SIGNAL] [pid#]
```

#### 1.4.3 ps - Processes
`ps` commands shows a snapshot of the currently running processes. The most commonly used options are `-e` and `-f` options. They can be merged together as `-ef`. So, the resultant command is like something like that `ps -ef` to see all running processes. The basic syntax is as follows.

```
ps [options]
```

## 2 Special Linux Features
### 2.1 File Descriptors
In Linux, there are 3 standard file descriptors. They are :

* stdin (0) - Standard input to the program
* stdout (1) - Standard output from the program
* stderr (2) - Standard error from the program

The default behavior suggests that input is from keyboard or a file. Both `stderr` and `stdout` go to the terminal. The default behaviour can be changed by file redirection operations.

### 2.2 File Redirection
The list of file redirection operators are given in the following table.

| Symbol | Redirection |
| :----: |-------------|
| `>`    | output redirect |
| `>>`   | append output |
| `|`    | pipe output to another command |
| `<`    | input redirection |
| `<<String` | read from `stdin` until `String` is encountered as the only thing on the line

**Example.** The following command concatenates `file1` and `file2` and redirects the output to the `file3`.

```
cat file1 file2 > file3
```

### 2.3 Other Special Command Symbols
In addition to file redirection symbols, there are numerous other symbols that you can use in Bourne shell. The following table shows some of them.

| Symbol | Meaning |
| :----: | ------- |
| `;`    | command seperator |
| `&`    | run the command in the background |
| `&&`   | run the command following this only if previous command completes successfully |
| `||`   | run the command following this only if previous command did not complete successfully |
| `()`   | the commands within the parantheses are executed in a subshell |
| `''`   | don't allow any special meaning to any characters within those quotes |
| `\`    | escape the following character |
| `""`   | regular quotation marks |

### 2.4 Wild Cards
The shell permits some special meaning characters. These are called **meta-characters** or **wild cards**. The following table lists some of them.

| Character | Meaning |
| :-------: | ------- |
| `?`       | match any single character at the indicated position |
| `*`       | match any string of zero or more characters |
| `[abc...]` | match any of the enclosed characters |
| `[a-e]`   | match any characters in the range a,b,c,d,e |
| `[!def]`  | match any characters not one of the enclosed characters |
| `~`       | home directory of the current user |

## 3 Text Processing
### 3.1 Regular Expression Syntax
Text processing commands such as *grep*, *sed*, *awk* uses regular expression. Notice that although it seems similar to the wild cards, some characters have entirely different meanings. The following table shows some important syntax elements.

| Character | Meaning |
| :-------: | ------- |
| `.`       | match any single character except newline |
| `*`       | match zero or more instances of the single character immediately preceding it |
| `[abc]`   | match any of the characters enclosed |
| `[a-d]`   | match any character in the enclosed range |
| `^[exp]`  | match any character not in the following expression |
| `^abc`    | the regular expression must start at the beginning of the line |
| `abc$`    | the regular expression must end at the end of line |
| `\`       | treat the next character literally |

### 3.2 Text Processing Commands
The three most important commands will be introduced in this section.

#### 3.2.1 grep - g/re/p
The `grep` command searches the given regular expression in the whole document and reports the lines that contains any matched string. The name `grep` comes from a `ed` command `g/re/p`. This command means that; search `re` globally (`g`). In other words, search `re` in each line. Print (`p`) the found lines. The general syntax is as follows.

```
grep [options] regexp [file(s)]
```

#### 3.2.2 sed - Stream Editor
*Todo:* This section will be implemented in some other time.

#### 3.2.3 awk - Aho, Weinberger, Kernighan
*Todo:* This section will be implemented in some other time.

## 4 Other Useful Commands
### 4.1 Working With Files
This section will introduce some very useful commands that can be found interesting while manipulating or analyzing files.

#### 4.1.1 cut - Cut Specified Fields
You can parse files in the column and row axes with this command. The basic syntax is as follows.

```
cut [options] [file(s)]
```

#### 4.1.2 find - Find Files Matching a Type or Pattern
The basic structure of the command is as follows.

```
find / -name foo.txt -type f -print
```

The first argument is the directory where the search starts. `-name foo.txt` is the name of the file that will be searched. `-type f` specifies what kind of file we search for. There is a list of supported types in the man page of the find command. In this case it is a *file*. Final part is the `-print` option. This part is optional unless you use it in a script.

The following command searches for a file called `foo.scala` in multiple directories.

```
find /opt /usr /var -name foo.scala -type f
```

Case insensitive search is same as the simple search except for a little difference.

```
find . -iname foo
find . -iname foo -type d
find . -iname foo -type f
```

For finding files with different extensions the following commands may be used.

```
find . -type f \( -name "*.c" -o -name "*.sh" \)
find . -type f \( -name "*cache" -o -name "*xml" -o -name "*html" \)
```

The `-o` option means logical OR.

To invert the result of a search pattern the following command might be used.

```
find . -type f -not -name "*.html"
```

Find the files that contains a text in them.

```
find . -type f -name "*.java" -exec grep -l StringBuffer {} \;
```

In the above example all files that ends with `.java` extension are given to the grep command. The end of the grep command is indicated by `;`. However, to protect the `;` from the unwanted shell interference there is a preceding `\`. The `{}` is just a placeholder where the files found by `find` can go.

To find and delete files with one command the following command is very useful.

```
find . -type f -name "Foo*" -exec rm {} \; 
```

#### 4.1.3 sort - Sort The Lines of a File
`sort` command can sort a file as a whole or with respect to a column that is delimited by a space.

**Exercise.** Sort the current directory with respect to the file size.

```
$ ls -al | sort -n -k5
```

For more options with sort you can check the manual page.

```
$ man sort
```

## 5 Shell Scripts
**Every shell run its own bash environment.** To run a script first thing to do is to make sure that the script has executable permission set. To make a script executable and run you can follow the steps below.

```
$ chmod u+x test.sh
$ ./test.sh
```

### 5.1 Shebang / Hashbang Line
This line specifies the which interpreter should be used to interpret the script.

```bash
#!/bin/bash
#!/bin/sh
#!/usr/bin/env python3
```

### 5.2 Variables and Comments
Comments are line of scripts that are not interpreted by the interpreter. In shell the comments are indicated by hash symbol `#`.

```bash
#!/bin/bash

# this is a comment
echo "Hello world" # This is another comment.
```

There are two types of variables in a Linux system. One is system variables and the other one is user defined variables. If a variable consist of capital letters, then most likely - but not necessarily - it is a system variable.

```bash
#!/bin/bash

echo "The shell name is $BASH"
echo "The shell version is $BASH_VERSION"
echo "Our home directory is $HOME"
echo "Current working directory is $PWD"

name="John"

echo $name
echo $name_wq

echo "The name is $name"
```

### 5.3 Reading User Inputs
Reading user input can be performed by the `read` command.

```bash
#!/bin/bash

echo "Enter name: "
read name
echo "Your name is $name"
```

You can take multiple inputs at the same time with the `read` command.

```bash
#!/bin/bash

echo "Enter multiple names: "
read name1 name2 name3
echo "Names: $name1, $name2, $name3"
```

However, becareful that the default delimiter character is the space, so you cannot give more than three strings to the above example. For example, the string `this is "a string that contains more than 3 words"` is actually parsed as three strings. The first one is `this`. The second one is `is`. And the rest of the string is quoted, so it is interpreted as a single string and will be assigned to the variable `name3`.

You can also combine an `echo` statement and a `read` statement via the `-p` option.

```bash
#!/bin/bash

read -p 'username: ' user_name
read -sp 'password: ' user_pass
echo "Username is $user_var"
echo "Password is $user_pass"
```

In above example, `read` first prints the string that is surrounded with quotes, then waits input from the user. You can also hide the input string while the user is typing with the `-s` option. Look for the man page of the `read` to get more information about `-s` option.

You can also get numerous inputs from the user with the `-a` option. `-a` option parses the given string and places all space delimeted words to the specified variable as an array.

```bash
#!/bin/bash

echo "Enter names: "
read -a names
echo "Names: ${names[0]}, ${names[1]}, ${names[2]}"
```

If the `read` command is called without any arguments than the input of the command can be reached over the `$REPLY` variable.

```bash
#!/bin/bash

echo "Enter name: "
read
echo "Name is $REPLY"
```

### 5.4 Passing Arguments to a Bash Script
The passed arguments to a bash script are stored in special variables `$1`, `$2`, `$3`, etc. In addition to those arguments, the `$0` variable holds the script / command name exactly how the script is called.

```bash
#!/bin/bash

# The first three arguments
echo $1 $2 $3

echo "The command name is $0"
```

To access the arguments list as an array, you can store the arguments to a user defined array variable. In some situations this example might be very useful.

```bash
#!/bin/bash

args=("$@")
echo ${args[1]} ${args[2]}
echo $@
```

You can get the number of arguments that is given to the script by using the `$#` variable.

```bash
#!/bin/bash

echo "Number of arguments is $#"
```

### 5.5 If Statement
The most basic branching statement in Bash is the if statement. Although the syntax may seem scary for a shell newbie, it is basically same with any other programming language such as C, C++, Java, etc. **Notice that** the spaces before and after the square brackets. Shell is not whitespace agnostic, so you need to make sure that you put those spaces to the correct places. Otherwise, you will get a syntax error for each whitespace error.

```bash
#!/bin/bash

if [ condition ]; then
    statement
fi

#----------------------------

if [ condition ]
then
    statement
fi
```

You can see the basic syntax of the if statement above. The both examples are valid. However, this manual follows the syntax of the second one.

#### 5.5.1 Integer Comparison Operators
You can use the following operators to construct a conditional expression.

* `-eq` : is equal to
* `-ne` : is not equal to
* `-gt` : is greater than
* `-ge` : is greater than or equal to
* `-lt` : is less than
* `-le` : is less than or equal to
* `<` : is less than (within double parentheses)
* `<=` : is less than or equal to (within double parentheses)
* `>` : is greater than (within double parentheses)
* `>=` : is greater than or equal to (within double parentheses)

The operators are not limited with the above items. You can find the full list of operators from *The Linux Documentation Project* web site.

For more informatio: http://tldp.org/LDP/abs/html/comparison-ops.html

The following example compares a variable with a number with two different syntax. The first one is POSIX standard of the `test` command and the second one is the arithmetic operations syntax.

```bash
#!/bin/bash

count=10

if [ $count -eq 9 ]
then
    echo "Condition is true"
fi

if (($count > 9))
then
    echo "Condition is true"
fi
```

The string comparison is similar to integer comparison. Instead of double equal signs, you can also use single equal sign. It will behave exactly same.

```bash
#!/bin/bash

word="abc"

if [ $word == "abc" ] # single equal sign is also valid
then
    echo "Condition is true"
fi
```

The `[` is not actually a syntax. It is just an alias for the `test` command. However, `[[` is a bash builtin syntax and much more powerful than the `[` command.

```bash
#!/bin/bash

word=a

if [[ $word < "b" ]]
then
    echo "Condition is true"
else
    echo "Condition is false"
fi
```

You can also use `else` and `elif` keywords as in other programming languages.

```bash
#!/bin/bash

word=a

if [[ $word == "b" ]]
then
    echo "Word is equal to b"
elif [[ $word == "a" ]]
then
    echo "Word is equalt to a"
else
    echo "Word is not equal to a or b"
fi
```

As a side note, `elif` and `else if` are actually same for the bash.

### 5.6 File Testing
Dealing with files is a very important part of Linux scripting. There are very huge amount of query options for file testing operations in bash. You can find the full list in [The Linux Documentation Project](https://www.tldp.org/LDP/abs/html/refcards.html#AEN22593) website.

The following example checks whether or not a file is exist in the current directory. If it is, prints file found. Otherwise, prints file not found.

```bash
#!/bin/bash

echo -e "Enter the name of the file: \c"
read fileName

# Check existense of a file
if [ -e $fileName ]
then
    echo "$fileName found."
else
    echo "$fileName not found"
fi

# Check for regular file: -f
# Check for directories: -d
```

Look man page of `echo` for `-e` flag.

**Example:** Check if a file exists and writable. If so, then write something in it.

```bash
#!/bin/bash

echo -e "Enter the name of the file: \c"
read fileName

if [ -f $fileName ]
then
    if [ -w $fileName ]
    then
        echo "Write some text. To quit press CTRL-D."
        cat >> $fileName
    else
        echo "You have not the write permission."
    fi
else
    echo "$fileName does not exist."
fi
```

**Example:** Write a script that checks if age is between 18 and 30.

```bash
#!/bin/bash

age=25

if [ $age -gt 18 ] && [ $age -lt 30 ]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi

#--------------------------------------

if [[ $age -gt 18 && $age -lt 30 ]]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi

#--------------------------------------

if [ $age -gt 18 -a $age -lt 30 ]
then
    echo "Age is valid."
else
    echo "Age is not valid."
fi
```

The `-a` option in the last section is shor for logical and operation. The same things are valid for or `||` operator.

### 5.7 Arithmetic Operations
The bash originally supports only integer arithmetic. However, you can find some tools to handle floating point arithmetic also such as `bc`.

#### 5.7.1 Integer Arithmetic
You need to tell the shell that you want to evaluate the numbers literally, not as strings. This can be done in several ways. One is using the double parantheses syntax inside a string. The following exercise shows double paratheses way.

**Exercise:** Implement a simple calculator. Write a script that takes three arguments. The first two are numbers and the last one is the operator.

```bash
#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Enter three arguments."
    exit 1
fi

num1=$1
num2=$2
operator=$3

if [[ $operator == "+" ]]
then
    echo "The result is $(( num1 + num2 ))"
elif [[ $operator == "-" ]]
then
    echo "The result is $(( num1 - num2 ))"
elif [[ $operator == "*" ]]
then
    echo "The result is $(( num1 * num2 ))"
elif [[ $operator == "/" ]]
then
    echo "The result is $(( num1 / num2 ))"
elif [[ $operator == "%" ]]
then
    echo "The result is $(( num1 % num2 ))"
else
    echo "Operator not found."
fi
```

Sample run:
```
$ ./test.sh 10 2 \&
Operator not found.

$ ./test.sh 10 2 +
The result is 12

$ ./test.sh 10 2 \*
The result is 20
```

Notice that you have to escape the star character (`*`) which is a wild card. The second way to evaluate mathematical expressions is using the `expr` command instead of double parantheses. For example, `echo $(expr $num1 + $num2)`.

#### 5.7.2 Floating Point Arithmetic
*Todo: will be implemented in some other time*

### 5.8 The Case Statement
The case statement is the Bash analogue of the switch statements in C style languages. The following example shows the basic syntax of a case statement.

```bash
#!/bin/bash

# Patterns are regular expressions.
case expression in
    pattern1 )
        statements
        ;;
    pattern2 )
        statements
        ;;
    ...
esac
```

Here `expression` might be a variable or a string. Patterns can be either string or regular expression.

**Exercise:** Write the previous calculator exercise with case statement.
```bash
#!/bin/bash

if [ $# -ne 3 ]
then
    echo "Enter three arguments."
    exit 1
fi

num1=$1
num2=$2
operator=$3

case $operator in
    "-" )
        echo "The result is $(expr $num1 - $num2)" ;;
    "+" )
        echo "The result is $(expr $num1 + $num2)" ;;
    "*" )
        echo "The result is $(expr $num1 \* $num2)" ;;
    * )
        echo "Unknown operator." ;;
esac
```

The last pattern (which is a `*` character) is wild card. It handles everything if none of the previous patterns match. It is the `default` keyword in other C style languages.

### 5.9 Array Variables

```bash
#! /bin/bash

os=('ubuntu' 'windows' 'manjaro' 'FreeBSD')
os[4]='mac'

unset os[1] # Remove the windows from the array

echo "${os[@]}" # Print all elements
echo "${os[0]}"
echo "${!os[@]}" # Print the indices of the array
echo "${#os[@]}" # Print length of the array
```

The `@` symbol inside and array index means array expansion. The order of elements are preserved if any of the elements are deleted via the `unset` command.

### 5.10 While Loops
The while loop is very similar to the other programming languages. It consists of two parts. The first part is condition of the loop. The loop runs as long as the condition is evaluated as true. The second part is the body of the loop. The body consists of only one command or multiple commands. The syntax is as follows.

```sh
#! /bin/bash

while [ condition ]
do
    command1
    command2
    ...
done
```

**Exercise:** Write a script that prints numbers from 1 to 10.

```sh
#! /bin/bash

n=1

while [ $n -le 10 ]
do
    echo "$n"
    # n=$(expr $n + 1)
    # n=$(( n + 1 ))
    (( n++ ))
    sleep 1
done

#-----------------------

while (( $n <= 10))
do
    echo $n
    (( n++ ))
done
```

Another very important usage of while loop is reading files line by line. You can pass a file to the loop either by using cat or file redirection operator. Then, you can read from file via `read` command as follows.

**Exercise:** Read the file with while loop.
```sh
while read line
do
    echo $line
done < test3.sh

#------------------

cat test3.sh | while read line
do
    echo $line
done
```

### 5.11 Until Loops
Until loop is almost same as while loop with a slight difference. If the condition is false, then the commands between do and done are executed.

```sh
#! /bin/bash

until [ condition ]
do
    command1
    command2
    ...
    commandN
done
```

**Exercise:** Print 1 to 10 to the shell.

```sh
#! /bin/bash

n=1

until [ $n -gt 10 ]
do
    echo $n
    (( n++ ))
done
```

### 5.12 For Loop
There are multiple versions of the for loop. The first one is executes commands that are given as space delimited list. The second one likes Python loops. It takes a range of numbers and loops from start to end by increasing the iteration variable by step. The final one is C style loop. The working principle of this type is completely same as C style for loops.

```sh
#! /bin/bash

for myVar in 1 2 3 4 5
do
    echo $myVar
done

#----------------------------

for myVar in {1..10..2} # {START..END..INCREMENT}
do
    echo $myVar
done

#----------------------------

for (( i=0; i<5; i++ ))
do
    echo $i
done
```

For can run several commands in sequence.

```sh
#! /bin/bash

for cmd in ls pwd date
do
    echo "--------------$cmd--------------"
    $cmd
done
```

For loops are very useful in iterating through files and directories. The following exercise shows how can you seperate regular files from directories.

**Exercise:** Print all directories in the folder.

```sh
#! /bin/bash

for item in *
do
    if [ -d "$item" ]
    then
        echo "$item"
    fi
done
```

### 5.13 Select Loop
To create text based menus, the select loop is the command you gonna use.

```sh
#! /bin/bash

select varName in list
do
    command1
    command2
    ...
    commandN
done
```

**Exercise:** Print 4 options and print the selected one.

```sh
#! /bin/bash

select name in mark john tom ben
do
    echo "$name selected."
done
```

Generally used with case statements.

```sh
#! /bin/bash

select name in mark john tom ben
do
    case $name in
        mark)
            echo "Mark selected." ;;
        *)
            echo "Error: please provide number between 1..4"
    esac
done
```

### 5.14 Functions

```sh
#! /bin/bash

function name() {
    command1
    command2
    ...
    commandN
}

name() {

}
```

**Exercise:** Write a script that prints hello world with functions.

```sh
#! /bin/bash

function hello() {
    echo "Hello"
}

quit() {
    exit 0
}

function print() {
    echo $1 # Print the first argument
}

# quit
print Hello
hello
```

### 5.15. Local Variables
All variable in a script are global variables. So, how to create a local variable.

```sh
#! /bin/bash

function print() {
    local name=$1
    echo "The name is $name"
}

name="Tom"
echo "The name is $name : Before"
print Max
echo "The name is $name : After"
```

**Exercise:** Check wheather a file exist or not with a user defined parameter. The parameter is the name of the file.

```sh
#! /bin/bash

function usage() {
    echo "You need to provide an argument : "
    echo "usage : $0 file_name"
}

function isFileExist() {
    local file="$1"
    [[ -f "$file" ]] && return 0 || return 1
}

[[ $# -eq 0 ]] && usage

if ( isFileExist "$1" )
then
    echo "File found"
else
    echo "File not found"
fi
```

Notice that 0 is true and 1 is false.

### 5.16 Constants
Constants are made with `readonly` command.

```sh
#! /bin/bash

var=30
readonly var

var=50
echo "var => $var"

function hello() {
    echo "Hello world"
}

readonly -f hello

function hello() {
    echo "Hello world from constant"
}

readonly
```

### 5.17 Signals and Traps
If you press CTRL-C while a script is running, then you sent an interrupt signal to the running process. The interrupt signal is also known as *SIGINT*.

```sh
#! /bin/bash

echo "PID is $$"
while (( COUNT < 10 ))
do
    sleep 10
    (( COUNT++ ))
    echo $COUNT
done
exit 0
```

CTRL-Z signal called suspend signal (a.k.a. SIGSTOP). This signal just pauses the current state of the process. Open the man page of the signals.

```
$ man 7 signal
```

Traps can be used to give responses to signals. For example:

```sh
#! /bin/bash

trap "echo Exit command is detected" 0
echo "Hello world"
exit 0
```

```sh
#! /bin/bash

trap "echo Interrupt signal is detected." SIGINT
echo "PID is $$"
while (( COUNT < 10 ))
do
    sleep 10
    (( COUNT++ ))
    echo $COUNT
done
exit 0
```

## 6 A Case Study
Build a menu system for a novice Linux user. There should be at least 4 menus consist of submenus and menus. The requirements are following.

1. User can see contents of a file.
2. User can remove a file.
3. User can copy a file.
4. User can list a directory.
5. User can display size of a file.
6. User can search a file for a pattern.
7. User can display line count, word count, and character count of a file.
8. User can display line differences between any two files.
9. User can display current date and time.
10. User can display current disk usage.
11. User can display details of current environment.
12. User can display a snapshot of process status information.

These requirements may be categorized as the following table.

File & Directory Management | Text Processing | System Status
--------------------------- | --------------- | -------------
See contents of a file | Search a file for a pattern | Display current date and time
Remove a file | Display line cnt, word cnt, char cnt | Current disk usage
Copy a file | Display line differences | Details of current environment
List a directory | - | Display process status info
Display size of a file | - | -

The program must consist of multiple shell scripts. **DO NOT PUT ALL PROGRAM INTO A SINGLE SCRIPT FILE**. All menus must also have an option to return the parent menu. Parent menu also must have an option to exit from the program.

Apart from above, the program must also support some command line options such as `file`, `text`, `status`. For example, if the script run with `file` option, the file management menu should come up.

**Sample Run**

```
$ ./novice_menu.sh
1) File and Directory Management Commands
2) Text Processing Commands
3) System Status Commands
4) Exit
Enter your choice: 1
1) Display the contents of a file  4) List a directory
2) Remove a file                   5) Size of a file
3) Copy a file                     6) Quit -- Return to main menu
Enter your choice: 1
File name: test.txt
This is a text file.
Second line
Third line
Forth line
Enter your choice: 5
File name: 3
stat: cannot stat '3': No such file or directory
Enter your choice: 6
Returning to main menu.
Enter your choice: 3
1) Display the current date and time   3) Display process status information
2) Current disk usage                  4) Quit -- Return to main menu
Enter your choice: 1
Cts Kas  3 14:17:06 +03 2018
Enter your choice: 4
Quitting.
Enter your choice: 4
Quitting.

$ ./novice_menu.sh status
1) Display the current date and time   3) Display process status information
2) Current disk usage                  4) Quit -- Return to main menu
Enter your choice: 4
Quitting.
1) File and Directory Management Commands
2) Text Processing Commands
3) System Status Commands
4) Exit
Enter your choice: 4
Quitting.
```

## 7 Synchronization
### 7.1 Threads in GNU/Linux
The following quote from the book *Linux Kernel Development, 3rd Edition* gives a great explanation of Linux threads.

> ...Linux has a unique implementation of threads. To the Linux kernel, there is no concept of a thread. Linux implements all threads as standard processes. The Linux kernel does not provide any special scheduling semantics or data structures to represent threads. Instead, a thread is merely a process that shares certain resources with other processes. Each thread has a unique task_struct and appears to the kernel as a normal process (which just happens to share resources, such as an address space, with other processes)...

And this quote continues with an example of a multi-threaded program.

> ...For example, assume you have a process that consists of four threads. On systems with explicit thread support (such as Windows, Solaris, ...), there might exist one process descriptor that in turn points to the four different threads. The process descriptor describes the shared resources, such as an address space or open files. The threads then describe the resources they alone possess. Conversely, in Linux, there are simply four processes and thus four normal `task_struct` structures. The four processes are set up to share certain resources...

### 7.2 POSIX Threads (pthread)
Pthread is a standard model for creating parallel subtasks. It comes with a C header file to include and a library to link with. It is one of the many thread models in the field. P in pthread comes from the POSIX (Portable Operating System Interface) which is the family of IEEE operating system interface standards.

The `pthread_t` is the structure which holds the id of any POSIX compliant thread. In a modern GNU/Linux system, any newly created thread is assigned to a core until the core number is reached. This structure guarantees that every thread runs concurrently without any interference to each other. After that, new threads are assigned to the same cores again and again. These threads are scheduled implicitly by the operating system.

To create a thread in a GNU/Linux system with the C language, the following procedure can be followed.

**Step 1** Creating the thread pointer by defining a `pthread_t` object.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int main(void) {
    pthread_t tid; // Holds the thread id.
    ...
    return 0;
}
```
Although it doesn't seem like a pointer, the `pthread_t` object is actually points to a thread. In other words, it carries the id of the thread. However, without any initialization step it doesn't hold anything.

**Step 2** Initializing and starting the `pthread_t` object.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* runner(void *args) {
    ...
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid; // Holds the thread id.
    pthread_create(&tid, NULL, runner, NULL); // Create and start the thread
    ...
    return 0;
}
```

Here the thread runs the function called `runner`. This function takes one `void*` argument and returns a `void*`. This feature is very important since `void*` can pass or return anything. It's a generic type to pass and return any type. The last argument of the `pthread_create` function is the arguments passed to the thread's `runner` function. Finally, when the `runner` function finished, the `pthread_exit` function returns a `void*` which is passed as its parameter and terminates the calling thread.

**Step 3** Waiting the running threads to finish before terminating the main program.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* runner(void *args) {
    ...
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid; // Holds the thread id.
    pthread_create(&tid, NULL, runner, NULL); // Create and start the thread
    ...
    pthread_join(tid, NULL); // Wait for the tid thread to finish.
    return 0;
}
```

The following quote from the Linux manual page of the `pthread_join` perfectly clarifies how to get the return status of a terminated thread.

> If the second parameter is not `NULL`, then `pthread_join` copies the exit status of the target thread into the location pointed to by the second parameter.

**Step 4** Compile and run the multi-threaded program.

```
$ gcc threads.c -pthread -std=c99 -Werror -o threads
$ ./threads
```

Above program is run with two threads. The first one is `main` thread and the second one is the `runner` thread. The `main` thread will wait for the `runner` thread to terminate before terminating itself.

#### 7.2.1 Exercises
**Exercise 1** Write a program that consists of 2 threads. One is waiting for 10 seconds. In the mean time the main thread counts from 1 to 10.

```C
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void* waiter(void *args) {
    printf("Waiter is sleeping...");
    sleep(10);
    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid;

    printf("Before thread\n");
    pthread_create(&tid, NULL, waiter, NULL);
    printf("After thread is started.");
    for (int i = 1; i < 10; i++) {
        printf("%d\n", i);
    }

    pthread_join(tid, NULL);
    printf("After thread.");

    return 0;
}
```

**Exercise 2** Write a program that prints numbers from 1 to 1000 and 1000 to 2000 simultaneously. Redirect the output of the program to a file called `output.txt`.

```C
#include <stdio.h>
#include <pthread.h>

void* runner(void *args) {
    int num;

    for (num = 1000; num <= 2000; num++) {
        printf("From runner: %d\n", num);
    }

    pthread_exit(NULL);
}

int main(void) {
    int num;
    pthread_t tid;
    pthread_create(&tid, NULL, runner, NULL);

    for (num = 1; num <= 1000; num++) {
        printf("From main: %d\n", num);
    }

    pthread_join(tid, NULL);
    return 0;
}
```

**Exercise 3** Write a program that calculates averages of two arrays simultaneously.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

struct CalcMod {
    int nums[1000];
    float result;
};

void* calculateAverage(void *args) {
    struct CalcMod *calcMod = (struct CalcMod*) args;
    float average = 0;

    for (int i = 0; i < 1000; i++) {
        average += calcMod->nums[i];
    }

    calcMod->result = average / 1000.0;

    pthread_exit(NULL);
}

int main(void) {
    pthread_t tid1, tid2;
    struct CalcMod m1, m2;

    for (int i = 0; i < 1000; i++) {
        m1.nums[i] = ((i + 1000) * 108) % 550;
        m2.nums[i] = ((i + 1000) * 202) % 708;
    }

    pthread_create(&tid1, NULL, calculateAverage, &m1);
    pthread_create(&tid2, NULL, calculateAverage, &m2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Result of first array: %.2f\n", m1.result);
    printf("Result of second array: %.2f\n", m2.result);

    return 0;
}
```

**Exercise 4** Analyze and run the following program to test for race conditions.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* updater(void *args) {
    int myid;
    myid = *((int*) args);
    //free(args);
    printf("My id is %d\n", myid);

    pthread_exit(NULL);
}

int main(void) {
    pthread_t tids[10];
    int *idPtr;

    for (int i = 0; i < 10; i++) {
        //idPtr = malloc(sizeof(int));
        //*idPtr = i;
        pthread_create(&tids[i], NULL, updater, &i/*idPtr*/);
    }

    for (int i = 0; i < 10; i++) {
        pthread_join(tids[i], NULL);
    }

    return 0;
}
```

**Exercise 5** Write a program that can calculate partial sums of a sequence simultaneously.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define ARRAY_LENGTH 100
#define THREAD_COUNT 5

struct sum_t {
    int numGroup[20];
    int result;
};

void* calculate(void *args) {
    struct sum_t *results = (struct sum_t*) args;
    results->result = 0;

    for (int i = 0; i < 20; i++) {
        results->result += results->numGroup[i];
    }
    pthread_exit(NULL);
}

int main(void) {
    int sum = 0;
    pthread_t tids[THREAD_COUNT];
    int numbers[ARRAY_LENGTH];
    struct sum_t results[THREAD_COUNT];

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        numbers[i] = i;
    }
    for (int i = 0; i < THREAD_COUNT; i++) {
        for (int j = 0; j < 20; j++) {
            results[i].numGroup[j] = numbers[j + i * 20];
        }

        if (pthread_create(&tids[i], NULL, calculate, &results[i])) {
            fprintf(stderr, "An error occured while creating thread %d.\n", i);
        }
    }

    for (int i = 0; i < THREAD_COUNT; i++) {
        pthread_join(tids[i], NULL);
        printf("Result of %d is %d.\n", i, results[i].result);
        sum += results[i].result;
    }
    printf("And the final result is %d.\n", sum);
    return 0;
}
```

### 7.3 Mutex Synchronization
Let's create a program that has two threads in addition to the main thread. The first one counts up and the second one counts down. However, both threads use the same variable. Again we will implement this program step by step.

**Step 1** Implement the program without any parallelism. One thread (main) counts up and counts down the `sum` variable. There is no race condition.

```C
#include <stdio.h>

#define NUM_LOOPS 20000000

int sum = 0;

void counting_function(int offset) {
    for (int i = 0; i < NUM_LOOPS; i++) {
        sum += offset;
    }
}

int main(int argc, char *args[]) {
    counting_function(1);
    counting_function(-1);

    printf("Sum is %d\n", sum);
    return 0;
}
```

**Step 2** Let's add some parallelism to the program by making count up and down operation parallel via two threads. Then, let's analyze the output of the program and compare it with the output of the previous program. Also measure the time spent with the `time` command.

```C
#include <stdio.h>
#include <pthread.h>

#define NUM_LOOPS 20000000

int sum = 0;

void* counting_function(void *args) {
    int offset = *((int*) args);
    for (int i = 0; i < NUM_LOOPS; i++) {
        // Critical section starts
        sum += offset;
        // Critical section ends
    }

    pthread_exit(NULL);
}

int main(int argc, char *args[]) {
    pthread_t tid1, tid2;
    int offset1 = 1, offset2 = -1;

    pthread_create(&tid1, NULL, counting_function, &offset1);
    pthread_create(&tid2, NULL, counting_function, &offset2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Sum is %d\n", sum);
    return 0;
}
```

**Step 3** We can prevent the race condition that we've set up in the previous step by protecting the critical section via a mutex.

```C
#include <stdio.h>
#include <pthread.h>

#define NUM_LOOPS 20000000

pthread_mutex_t mutex_sum;

int sum = 0;

void* counting_function(void *args) {
    int offset = *((int*) args);
    for (int i = 0; i < NUM_LOOPS; i++) {
        // Critical section starts
        pthread_mutex_lock(&mutex_sum);
        sum += offset;
        pthread_mutex_unlock(&mutex_sum);
        // Critical section ends
    }

    pthread_exit(NULL);
}

int main(int argc, char *args[]) {
    pthread_t tid1, tid2;
    pthread_mutex_init(&mutex_sum, NULL);
    int offset1 = 1, offset2 = -1;

    pthread_create(&tid1, NULL, counting_function, &offset1);
    pthread_create(&tid2, NULL, counting_function, &offset2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Sum is %d\n", sum);
    pthread_mutex_destroy(&mutex_sum);
    return 0;
}
```

Notice that the time difference between with mutex and without mutex. This is because with mutex, each thread that wants to enter the critical section must wait until the previous thread finishes its operations inside the critical section. This situation similar to the without threads one in step 1.

#### 7.3.1 Deadlocks via Mutexes
The following paragraph is quoted from the *Operating System Concepts, 9th Edition, Silberschatz A.*.

> ...In a multiprogramming environment, several processes may compete for a finite number of resources. A process requests resources; if the resources are not available at that time, the process enters a waiting state. Sometimes, a waiting process is never again able to change state, because the resources it has requested are held by other waiting processes. This situating is called a **deadlock**...

With this information in mind, let's implement a situation that produces a deadlock.

```C
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t first_mutex;
pthread_mutex_t second_mutex;

void* do_work_one(void *args) {
    pthread_mutex_lock(&first_mutex);
    //sleep(5);
    pthread_mutex_lock(&second_mutex);
    // Critical section begins
    printf("Critical section of work one.\n");
    // Critical section ends
    pthread_mutex_unlock(&second_mutex);
    pthread_mutex_unlock(&first_mutex);

    pthread_exit(NULL);
}

void* do_work_two(void *args) {
    pthread_mutex_lock(&second_mutex);
    pthread_mutex_lock(&first_mutex);
    // Critical section begins
    printf("Critical section of work two.\n");
    // Critical section ends
    pthread_mutex_unlock(&first_mutex);
    pthread_mutex_unlock(&second_mutex);

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t tid1, tid2;
    pthread_mutex_init(&first_mutex, NULL);
    pthread_mutex_init(&second_mutex, NULL);

    pthread_create(&tid1, NULL, do_work_one, NULL);
    pthread_create(&tid2, NULL, do_work_two, NULL);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    pthread_mutex_destroy(&first_mutex);
    pthread_mutex_destroy(&second_mutex);

    return 0;
}
```

In this example, there is a chance that a deadlock occurs. If `tid1` acquires `first_mutex` while `tid2` acquires `second_mutex`, a deadlock will occur. Although there is a chance that a deadlock occurs, most of the time deadlock won't occur if `tid1` can acquire and release the mutex locks for `first_mutex` and `second_mutex` before `tid2` attempts to acquire mutexes. However, we can force to occur a deadlock with `sleep()` function from `unistd.h` library.

### 7.4 Semaphore Synchronization
Semaphores are little bit more complicated then the mutexes in a way that they can take more then two values. Simply, a **semaphore** is a an integer variable that can be accessed only through two atomic operations: `wait()` and `signal()`. As a result, we can use semaphores in a lot more places than we can use mutexes.

In C semaphores are implemented in `semaphore.h` header file. `sem_t` is the name of the semaphore type definition. `sem_wait()` and `sem_post()` functions are the analogues of the `wait()` and `signal()` functions respectively. Let's inspect the **Step 3** of section 7.3. with semaphores.

**Exercise.** Check for the race condition.

```C
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define NUM_LOOPS 20000000

sem_t sem;

int sum = 0;

void* counting_function(void *args) {
    int offset = *((int*) args);
    for (int i = 0; i < NUM_LOOPS; i++) {
        // Critical section starts
        sem_wait(&sem);
        sum += offset;
        sem_post(&sem);
        // Critical section ends
    }

    pthread_exit(NULL);
}

int main(int argc, char *args[]) {
    pthread_t tid1, tid2;
    sem_init(&sem, 0, 1);
    int offset1 = 1, offset2 = -1;

    pthread_create(&tid1, NULL, counting_function, &offset1);
    pthread_create(&tid2, NULL, counting_function, &offset2);

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    printf("Sum is %d\n", sum);
    sem_destroy(&sem);
    return 0;
}
```

#### 7.4.1 Readers - Writers Problem
*Thread and Semaphore Examples, Zelenski, J. & Parlante, N.*:

>In this classic Reader - Writer problem, there are two threads exchanging information through a fixed size buffer. The Writer thread fills the buffer with data whenever there's room for more. The Reader thread reads data from the buffer and prints it. Both threads have a situation where they should block. The writer blocks when the buffer is full and the reader blocks when the buffer is empty. The problem is to get them to cooperate nicely and block efficiently when necessary.

```C
/**
 * readers_writers.c
 * -----------------
 * Thread and Semaphore Examples
 * Julie Zelenski, Nick Parlante
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#define NUM_TOTAL_BUFFERS 5
#define DATA_LENGTH 20

sem_t empty, full;
char buffers[NUM_TOTAL_BUFFERS];

char prepareData() {
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);
    char letter = (rand() % 26) + 65;
    return letter;
}

void processData() {
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);
}

void* writer_routine(void *args) {
    int writePt = 0;
    char data;

    for (int i = 0; i < DATA_LENGTH; i++) {
        data = prepareData();
        sem_wait(&empty);
        buffers[writePt] = data;
        printf("%s: buffer[%d] = %c\n", "Writer", writePt, data);
        writePt = (writePt + 1) % NUM_TOTAL_BUFFERS;
        sem_post(&full);
    }

    pthread_exit(NULL);
}

void* reader_routine(void *args) {
    int readPt = 0;
    char data;

    for (int i = 0; i < DATA_LENGTH; i++) {
        sem_wait(&full);
        data = buffers[readPt];
        printf("\t\t%s: buffer[%d] = %c\n", "Reader", readPt, data);
        readPt = (readPt + 1) % NUM_TOTAL_BUFFERS;
        sem_post(&empty);
        processData();
    }

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t writer, reader;
    srand(time(NULL));

    sem_init(&empty, 0, NUM_TOTAL_BUFFERS);
    sem_init(&full, 0, 0);

    pthread_create(&writer, NULL, writer_routine, NULL);
    pthread_create(&reader, NULL, reader_routine, NULL);

    pthread_join(writer, NULL);
    pthread_join(reader, NULL);

    sem_destroy(&empty);
    sem_destroy(&full);

    printf("All done!\n");
    return 0;
}
```

#### 7.4.2 Dining Philosophers Problem
*Thread and Semaphore Examples, Zelenski, J. & Parlante, N.*:

>Another classic concurrency problem concerns a group of philosophers seated about a round table eating spaghetti. There are the same total number of forks as there are philosophers and one fork is placed between every two philosophers-that puts a single fork to the left and right of each philosopher. The philosophers run the traditional **think-eat** loop. After he sits quietly thinking for a while, a philosopher gets hungry. To eat a philosopher grabs the fork to the left, grabs the fork to the right, eat for a time using both forks, and then replaces the forks and goes back to thinking.

There is a possible deadlock condition if the philosophers are allowed to grab forks freely. The deadlock occurs if no one is eating, and then all the philosophers grab the fork to their left, and then look over and wait for the right fork.

```C
/**
 * dining_philosophers.c
 * -----------------
 * Thread and Semaphore Examples
 * Julie Zelenski, Nick Parlante
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>
#include <unistd.h>

#define NUM_DINNERS 5
#define EAT_TIMES 3

#define LEFT(philNum) (philNum)
#define RIGHT(philNum) (((philNum) + 1) % NUM_DINNERS)

struct thread_data {
    char name[32];
    int philNum;
};

sem_t fork_sem[NUM_DINNERS], numEating;

void think(char *threadName) {
    printf("%s thinking!\n", threadName);
    int sleepTime = sleep((rand() % 5) + 1); 
    sleep(sleepTime);
}

void eat(struct thread_data *data) {
    sem_wait(&numEating);
    sem_wait(&fork_sem[LEFT(data->philNum)]);
    sem_wait(&fork_sem[RIGHT(data->philNum)]);

    printf("%s eating!\n", data->name);
    int sleepTime = (rand() % 5) + 1;
    sleep(sleepTime);

    sem_post(&fork_sem[LEFT(data->philNum)]);
    sem_post(&fork_sem[RIGHT(data->philNum)]);
    sem_post(&numEating);
}

void* philosopher(void *args) {
    struct thread_data *data = (struct thread_data*) args;

    for (int i = 0; i < EAT_TIMES; i++) {
        think(data->name);
        eat(data);
    }

    free(data);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t philosophers[NUM_DINNERS];
    srand(time(NULL));

    for (int i = 0; i < NUM_DINNERS; i++) {
        sem_init(&fork_sem[i], 0, 1); // Fork mutexes
    }
    sem_init(&numEating, 0, NUM_DINNERS - 1);

    for (int i = 0; i < NUM_DINNERS; i++) {
        struct thread_data *data = malloc(sizeof(struct thread_data));
        sprintf(data->name, "Philosopher %d", i);
        data->philNum = i;
        pthread_create(&philosophers[i], NULL, philosopher, data);
    }

    for (int i = 0; i < NUM_DINNERS; i++) {
        pthread_join(philosophers[i], NULL);
    }

    printf("All done!\n");

    for (int i = 0; i < NUM_DINNERS; i++) {
        sem_destroy(&fork_sem[i]);
    }
    sem_destroy(&numEating);

    return 0;
}
```

## 8 Communication Over Sockets in C
Socket programming in C might appear little scary and complicated for a beginner. Inversely, the steps of socket programming is very well defined. If you understand the steps, you can easily implement a socket communication example. Before everything else, let's analyze the following diagram.

![Client Server in Unix](client-server-in-unix.png)

**Step 1** As we discuss earlier, every thing in a GNU/Linux system is a file. Our socket is a file. So, we need a socket file descriptor to represent the socket that we will create. There are some system calls and wrappers to help us to create a socket. The following code example shows how to create a socket.

```C
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

void init_tcp_server(int port) {
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    ...
}
```

**Step 2** Next a port should be bound to the newly created socket.

```C
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

void init_tcp_server(int port) {
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port); // Convert host to network byte order
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    addrlen = sizeof(struct sockaddr_in);

    bind(sockfd, (struct sockaddr*) &address, sizeof(address));
}
```

**Step 3** Now we are ready to listen to the port for incoming connection requests.

```C
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

void init_tcp_server(int port) {
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port); // Convert host to network byte order
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    addrlen = sizeof(struct sockaddr_in);

    bind(sockfd, (struct sockaddr*) &address, sizeof(address));
    listen(sockfd, 10); // 10 is the queue length
}
```

**Step 4** When the socket listens for incoming connections, all of the connection requests are inserted to a queue. Thus, we need to get a request and accept its connection request. As a side note, none of the above functions are blocking. However, the `accept()` function is blocking. It means the program will actually wait for a connection request.

```C
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

void init_tcp_server(int port) {
    int sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port); // Convert host to network byte order
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    addrlen = sizeof(struct sockaddr_in);

    bind(sockfd, (struct sockaddr*) &address, sizeof(address));
    listen(sockfd, 10); // 10 is the queue length
    struct sockaddr client_addr;
    int nsockfd = accept(sockfd, &client_addr, &socklen);
}
```

`accept()` function gets the server socket file descriptor, a structure to put the client information in it and a variable to put the size of the structure that holds the client information. The last argument is actually a value-result argument. The function returns a socket file descriptor that belongs to the newly connected client on success.

At this point, we can send and receive any data by using the client socket file descriptor with the functions `send()` and `recv()`. You can find the more detailed information in manual pages of these functions.

### 8.1. A Single Client-Server Communication Example
The following program demonstrates how a fully functional server communicates with a single client. The server gets some data from the client and mirrors it back to the client.

```C
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

void print_error_terminate(const char *function_name);
int run = 0;

void signal_handler(int signum) {
    printf("\nSIGINT catched.\nStarting termination process...\n");
    run = 0;
}

int init_tcp_server(int port) {
    int status;
    struct sockaddr_in addr;
    int srv_sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    signal(SIGINT, signal_handler);
    if (srv_sockfd == -1) print_error_terminate("socket");

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    status = bind(srv_sockfd, (struct sockaddr*) &addr, sizeof(addr));
    if (status == -1) print_error_terminate("bind");

    return srv_sockfd;
}

void start_listen(int srv_sockfd) {
    int status = listen(srv_sockfd, 5);
    if (status == -1) print_error_terminate("listen");
}

void accept_connections(int srv_sockfd) {
    run = 1;

    while (run) {
        char buffer[32] = { 0 };
        int status;
        struct sockaddr client_addr;
        int socklen = sizeof(struct sockaddr_in);
        int client_sockfd = accept(srv_sockfd, &client_addr, &socklen);

        status = recv(client_sockfd, buffer, 32, 0);
        if (status == -1) print_error_terminate("recv");

        status = send(client_sockfd, buffer, strlen(buffer) + 1, 0);
        if (status == -1) print_error_terminate("send");

        close(client_sockfd);
    }

    close(srv_sockfd);
}

void print_error_terminate(const char *function_name) {
    int error_code = errno;
    fprintf(stderr, "Call to %s() failed with error code %d.\n", function_name, error_code);
    exit(-1);
}

int main(int argc, char *argv[]) {
    int sockfd = init_tcp_server(8080);
    start_listen(sockfd);
    accept_connections(sockfd);

    return 0;
}
```

To interact with the server, you can use the *netcat* programg. You can host a very simple server as well as you can connect to a server as a client with *netcat*. The following command connects to the specified server.

```
$ nc 127.0.0.1 8080
```

The short name of the *netcat* is `nc`. The first argument is the address of the server and the second argument is the port to connect. When you connect the above server with the above command, you might see something like below.

```
$ nc 127.0.0.1 8080
This is a test.
This is a test.
```

This is because our server just mirrors what it reads from the TCP stream.

## 9 Parallel Computing With OpenMP
OpenMP is the most commonly used parallel programming API in industry. The learning curve of the OpenMP is realtively easy. You don't necessarily spent a lot of time to learn it. There is no complex language notations or complex API. OpenMP can get you through most of the hard staff of parallel computing quickly. Thus, you can jump into the more enjoying parts of parallel algorithms.

OpenMP is originally made in C and it supports C++ and Fortran. You need to know little bit about parallel programming.

### 9.1 Getting Started With OpenMP
The first thing to do in this section is to make the distinction between *concurrency* and *parallelism* clear. *Concurrency* is a condition of a system in which multiple tasks are logically active at one time. *Parallelism* is a condition of a system in which multiple tasks are actually actually active at one time. One example to this explanation might be if your computer has a processor with only one core, then all of your processes are running concurrenly. On the other hand, if your processor has multiple cores, then some of the processes might run parallel.

Another similar concept might come up with the name *concurrent applications* and *parallel applications*. *Concurrent applications* make computations **logically** execute simultaneously due to the semantics of the application. In *concurrent applications*, the problem is fundamentally concurrent. A web server might be an example for that. A *parallel application* make computations **actually** execute simultaneously in order to complete a problem in less time. In *parallel applications*, the problem doesn't inherently require concurrency. You can state it sequentially.

OpenMP is an API for writing multi-threaded applications. It is a set of compiler directives and library routines for parallel application programmers. Greatly simplifies writing multi-threaded (MT) programs in Fortran, C and C++.

The most of the constructs in OpenMP are compiler directives.

```C
#include <omp.h>
#pragma omp construct [clause [clause]...]
#pragma omp parallel num_threads(4)
```

Most OpenMP constructs apply to a *structured block*. A *structured block* is a block of one or more statements with one point of entry at the top and one point of exit at the bottom.

To compile programs with OpenMP in GNU/Linux environment you don't need to do anything special. Just put the `-fopenmp` flag while compiling your program.

```sh
$ export OMP_NUM_THREADS=4
$ gcc -fopenmp foo.c
$ ./a.out
```

Now, you need to write a simple "hello world" program and run it as the following example.

```C
#include <stdio.h>

int main() {
    int id = 0;

    printf("Hello(%d)", id);
    return 0;
}
```

```
$ gcc foo.c
$ ./a.out
Hello(0)
```

After make sure you run the program, modify the program as follows and run.

```C
#include <stdio.h>
#include <omp.h>

int main() {
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        printf("Hello(%d)", id);
        printf(" world(%d)\n", id);
    }
    return 0;
}
```

```
$ gcc foo.c -fopenmp
$ ./a.out
Hello(1) world(1)
Hello(2) world(2)
Hello(0) world(0)
Hello(3) world(3)
```

### 9.2 The Core Features of OpenMP
### 9.3 Working With OpenMP
### 9.4 Advenced OpenMP
