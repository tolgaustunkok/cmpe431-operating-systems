#!/bin/bash

for hw_dir in *
do
    if [[ -d "$hw_dir" ]]
    then
        #echo "$hw_dir/"*.c
        gcc -pthread -std=c99 "$hw_dir/"*.c -o "$hw_dir/compiled"
    fi
done
