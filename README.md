# CMPE431 - Operating Systems
This repository holds the operating systems laboratory activities and quizes.

## Getting Started
The project includes multiple markdown files and Bash scripts. To run them at least Bash 4.* is mandatory.

## Authors
* **Tolga Üstünkök** - *Research Assistant* - [Atılım University Department of Software Engineering](https://www.atilim.edu.tr/en/se)
* **Ersin Tiryaki** - *Research Assistant* - [Atılım University Department of Software Engineering](https://www.atilim.edu.tr/en/se)